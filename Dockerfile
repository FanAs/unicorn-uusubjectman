FROM node:14

WORKDIR /app

ENTRYPOINT [""]

ADD . /app

RUN rm -rf node_modules && npm install

WORKDIR /app/client

RUN rm -rf node_modules && npm install && npm run-script build

WORKDIR /app

USER 1000:1000

EXPOSE 3000
CMD ["npm", "start"]
