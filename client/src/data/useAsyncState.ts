import {Dispatch, SetStateAction, useEffect, useState} from "react"

export type LoadingState<T, E = any> = {
	isLoading: boolean;
	isLoaded: boolean;
	data: T;
	error: E | undefined;
}

export const useAsyncState = <S>(initialState: any, initialData?: S | (() => Promise<S>), params?: { repeatInterval?: number }): [LoadingState<S>, (newValue: S | (() => Promise<S>)) => void, Dispatch<SetStateAction<LoadingState<S>>>] => {
	const [state, setState] = useState<LoadingState<S>>({isLoading: false, isLoaded: false, data: initialState, error: undefined});

	const fn = (value: S | (() => Promise<S>)): void => {
		if (typeof value === 'function') {
			// @ts-ignore
			value = value();
		}

		setState({isLoading: true, isLoaded: false, data: state.data, error: undefined});
		(value as unknown as Promise<any>).then((result) => {
			setState({isLoading: false, isLoaded: true, data: result, error: undefined});
		}).catch((err) => {
			// @ts-ignore

			setState({isLoading: false, isLoaded: false, error: err.response?.data ?? err, data: undefined});

			if (params?.repeatInterval != null) {
				setTimeout(() => {
					fn(value);
				}, params.repeatInterval);
			}
		});
	}

	useEffect(() => {
		if (state.isLoading || state.isLoaded || state.error != null) {
			return;
		}

		if (initialData !== undefined) {
			if (typeof initialData === 'function') {
				// @ts-ignore
				initialData = initialData();
			}

			// @ts-ignore
			fn(initialData);
		}
	})

	return [state, fn, setState as any];
}