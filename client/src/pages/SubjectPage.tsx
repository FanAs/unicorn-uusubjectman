import React, {useState} from "react"
import {useParams} from "react-router-dom"
import {TLanguage} from "../../../src/api/models/TLanguage"
import {TSubjectTranslation} from "../../../src/api/models/TSubjectTranslation"
import { useAsyncState } from "../data/useAsyncState";
import { request } from "gaxios";
import {Box, Select} from "grommet"
import {LanguagesSelect} from "../features/language/LanguagesSelect"
import {TranslationPreview} from "../features/subject/TranslationPreview"
import {SubjectPreview} from "../features/subjects/subjectPreview"
import {Async, AsyncLoading, AsyncRejected, AsyncResolved} from "../features/util/Async"
import {ErrorMessage} from "../features/util/Error"
import {Spinner} from "../features/util/Spinner"
import { Page } from "./Page";

export function SubjectPage() {
    const { id } = useParams<{id: string}>();

    const [subject, _, setSubject] = useAsyncState(null ,async () => {
        const {data} =  await request<TSubjectTranslation[]>({
            url: `/api/translations/${id}`,
        });

        return data;
    });

    return(
         <Page>
             <Async state={subject}>
                 <AsyncLoading><Spinner /></AsyncLoading>
                 <AsyncResolved>
                     {(data: typeof subject.data) =>
                         <TranslationPreview translations={data} onDelete={(translation) => {
                             setSubject({...subject, data: subject.data.filter((tr: any) => tr !== translation)})
                         }} />
                     }
                 </AsyncResolved>
                 <AsyncRejected>{(err: any) => <ErrorMessage>{err.message}</ErrorMessage>}</AsyncRejected>
             </Async>
         </Page>
    )}