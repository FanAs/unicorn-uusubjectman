import {Box, Grommet} from "grommet"
import React from "react"
import styled from "styled-components"
import {theme} from "../styles/theme"
import {NavigationBar} from "../features/navigation/NavigationBar"
import {Sidebar} from "../features/navigation/Sidebar"

const StyledBox = styled(Box)`
	padding-left: 60px;
	@media screen and (min-width: 800px) {
		padding-left: 220px;
	}
`

export function Page(props: { children: any }) {
	return (
		<Grommet full theme={theme}>
			<Box fill>
				<NavigationBar />
				<Box direction="row" flex overflow={"auto"}>
					<Sidebar />
					<StyledBox margin={{ horizontal: 'small'}} fill>
						{props.children}
					</StyledBox>
				</Box>
			</Box>
		</Grommet>
	)
}