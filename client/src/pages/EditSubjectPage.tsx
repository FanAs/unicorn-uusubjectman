import {request} from "gaxios"
import { Box } from "grommet"
import React, {useState} from "react"
import {useHistory, useParams} from "react-router-dom"
import {TDegree} from "../../../src/api/models/TDegree"
import {TSubject} from "../../../src/api/models/TSubject"
import {useAsyncState} from "../data/useAsyncState"
import {DegreesSelect} from "../features/degree/DegreesSelect"
import {CreateSubjectComponent} from "../features/subject/CreateSubjectComponent"
import {Async, AsyncLoading, AsyncRejected, AsyncResolved} from "../features/util/Async"
import {ErrorMessage} from "../features/util/Error"
import {Spinner} from "../features/util/Spinner"
import {Page} from "./Page"

export function EditSubjectPage() {
	const history = useHistory();
	const {id} = useParams<{id: string}>();

	const [subject] = useAsyncState<TSubject>(null, async () => {
		const {data} = await request<TSubject>({
			url: `/api/subject/${id}`,
			method: 'GET',
		});

		return data;
	});

	return (
		<Page>
			<Box width={"large"} alignSelf={"center"}>
				<Async state={subject}>
					<AsyncLoading><Spinner /></AsyncLoading>
					<AsyncResolved>
						{(data: typeof subject.data) =>
							<Box gridArea="degree" pad={{vertical: 'xsmall'}}>
								<CreateSubjectComponent value={data} onSubmit={() => history.push(`/`)} />
							</Box>
						}
					</AsyncResolved>
					<AsyncRejected>{(err: any) => <ErrorMessage>{err.message}</ErrorMessage>}</AsyncRejected>
				</Async>
			</Box>
		</Page>
)
}