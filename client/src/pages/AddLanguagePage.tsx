import { Box } from "grommet"
import React from "react"
import {useHistory} from "react-router-dom"
import {CreateTranslationComponent} from "../features/subject/CreateTranslationComponent"
import {Page} from "./Page"

export function AddLanguagePage() {
	const history = useHistory();

	return (
		<Page>
			<Box width={"large"} alignSelf={"center"}>
				<CreateTranslationComponent onSubmit={(lang, id) => history.push(`/subject/${lang.id}/${id}`)} />
			</Box>
		</Page>
)
}