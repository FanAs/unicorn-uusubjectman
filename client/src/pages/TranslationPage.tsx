import {Box} from "grommet"
import React from "react"
import {useParams} from "react-router-dom"
import {TSubjectTranslation} from "../../../src/api/models/TSubjectTranslation"
import { useAsyncState } from "../data/useAsyncState";
import { request } from "gaxios";
import {TSubject} from "../../../src/api/models/TSubject"
import {Translation} from "../features/subject/Translation"
import {SubjectPreview} from "../features/subjects/subjectPreview"
import {Async, AsyncLoading, AsyncRejected, AsyncResolved} from "../features/util/Async"
import {ErrorMessage} from "../features/util/Error"
import {Spinner} from "../features/util/Spinner"
import { Page } from "./Page";

export function TranslationPage() {
    const { translationId } = useParams<{translationId: string}>();

    const [subject] = useAsyncState(null ,async () => {
        const {data} =  await request<TSubjectTranslation>({
            url: `/api/translation/${translationId}`,
        });

        return data;
    });

    return(
         <Page>
             <Async state={subject}>
                 <AsyncLoading><Spinner /></AsyncLoading>
                 <AsyncResolved>
                     {(data: typeof subject.data) =>
                         <Translation translation={data} />
                     }
                 </AsyncResolved>
                 <AsyncRejected>{(err: any) => <ErrorMessage>{err.message}</ErrorMessage>}</AsyncRejected>
             </Async>
         </Page>
    )}