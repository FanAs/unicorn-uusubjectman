import React from "react"
import {TAuditLog} from "../../../src/api/models/TAuditLog"
import { useAsyncState } from "../data/useAsyncState";
import { request } from "gaxios";
import {AuditLog} from "../features/auditLog/AuditLog"
import {Async, AsyncLoading, AsyncRejected, AsyncResolved} from "../features/util/Async"
import {ErrorMessage} from "../features/util/Error"
import {Spinner} from "../features/util/Spinner"
import { Page } from "./Page";

export function AuditLogPage() {
    const [auditLog] = useAsyncState([] ,async () => {
        const {data} =  await request<TAuditLog[]>({
            url: '/api/auditLog',
            params: {},
        });

        return data;
    });

    return(
         <Page>
             <Async state={auditLog}>
                 <AsyncLoading><Spinner /></AsyncLoading>
                 <AsyncResolved>
                     {(data: typeof auditLog.data) =>
                         <AuditLog data={data} />
                     }
                 </AsyncResolved>
                 <AsyncRejected>{(err: any) => <ErrorMessage>{err.message}</ErrorMessage>}</AsyncRejected>
             </Async>
         </Page>
    )}