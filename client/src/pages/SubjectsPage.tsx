import React from "react"
import { useAsyncState } from "../data/useAsyncState";
import { request } from "gaxios";
import {TSubject} from "../../../src/api/models/TSubject"
import {Async, AsyncLoading, AsyncRejected, AsyncResolved} from "../features/util/Async"
import {ErrorMessage} from "../features/util/Error"
import {Spinner} from "../features/util/Spinner"
import { Page } from "./Page";
import { Subjects } from "../features/subjects/subjects";

export function SubjectsPage() {
    const [subjects, _, setSubjects] = useAsyncState<any>([] ,async () => {
        const {data} =  await request<TSubject[]>({
            url: '/api/subjects',
            params: {},
        });

        return data;
    }, { repeatInterval: 5000 });

    return(
         <Page>
             <Async state={subjects}>
                 <AsyncLoading><Spinner /></AsyncLoading>
                 <AsyncResolved>
                     {(data: typeof subjects.data) =>
                         <Subjects onDelete={(subject) => {
                            setSubjects({...subjects, data: data.filter((sub: TSubject) => sub !== subject)})
                         }
                         } data={data} />
                     }
                 </AsyncResolved>
                 <AsyncRejected>{(err: any) => <ErrorMessage>{err.statusCode === 431 ? 'You need to sign in' : err.message}</ErrorMessage>}</AsyncRejected>
             </Async>
         </Page>
    )}