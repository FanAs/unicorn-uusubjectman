import { Box } from "grommet"
import React from "react"
import {useHistory} from "react-router-dom"
import {CreateSubjectComponent} from "../features/subject/CreateSubjectComponent"
import {Page} from "./Page"

export function CreateSubjectPage() {
	const history = useHistory();

	return (
		<Page>
			<Box width={"large"} alignSelf={"center"}>
				<CreateSubjectComponent onSubmit={(id) => history.push(`/`)} />
			</Box>
		</Page>
)
}