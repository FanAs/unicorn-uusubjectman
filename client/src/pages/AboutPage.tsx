import {Box, Text, Heading, Image} from "grommet"
import React from "react"
import {Page} from "./Page"
import { AuthorImage } from "../features/about/AuthorImage"


export function AboutPage() {
	return (
		<Page>
			<Box align={'center'}>
				<Heading>uuSubjectMan</Heading>
				<Text>Created by</Text>
				<Box direction={"row"} pad={"medium"} gap={"medium"}>
					<Box align={"center"}>
						<AuthorImage src={"/images/jan-vokroj.png"} />
						Jan Vokroj
					</Box>
					<Box align={"center"}>
						<AuthorImage src={"/images/marija-lebedeva.png"} />
						Marija Lebeděva
					</Box>
					<Box align={"center"}>
						<AuthorImage src={"/images/tomas-jindra.png"} />
						Tomáš Jindra
					</Box>
					<Box align={"center"}>
						<AuthorImage src={"/images/artyom-suchkov.png"} />
						Artyom Suchkov
					</Box>
				</Box>
			</Box>
		</Page>
	);
}