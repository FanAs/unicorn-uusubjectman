import {request} from "gaxios"
import { Box } from "grommet"
import React from "react"
import {useHistory, useParams} from "react-router-dom"
import {TSubjectTranslation} from "../../../src/api/models/TSubjectTranslation"
import {useAsyncState} from "../data/useAsyncState"
import {CreateTranslationComponent} from "../features/subject/CreateTranslationComponent"
import {Page} from "./Page"

export function EditLanguagePage() {
	const history = useHistory();
	const {id} = useParams<{id: string}>();

	const [translation] = useAsyncState<TSubjectTranslation>(null, async () => {
		const {data} = await request<TSubjectTranslation>({
			url: `/api/translation/${id}`,
			method: 'GET',
		});

		return data;
	});

	if (!translation.isLoaded) {
		return null;
	}

	return (
		<Page>
			<Box width={"large"} alignSelf={"center"}>
				<CreateTranslationComponent value={translation.data} onSubmit={(lang, id) => history.push(`/subject/${lang.id}/${id}`)} />
			</Box>
		</Page>
)
}