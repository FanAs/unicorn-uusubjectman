import {Box, Heading} from "grommet"
import React from "react"
import {Page} from "./Page"

export function NotFoundPage() {
	return (
		<Page>
			<Box justify={'center'} align={'center'} direction={'row'}>
				<Box>
					<img src={"/images/yH.gif"} />
				</Box>
				<Heading>Page not found</Heading>
			</Box>
		</Page>
	)
}