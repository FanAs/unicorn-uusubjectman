export const theme = {
	"name": "my theme",
	"rounding": 2,
	"spacing": 20,
	"defaultMode": "light",
	"global": {
		"focus": {
			"border": {
				"color": "rgba(0, 0, 0, 0)",
			},
		},
		"heading": {
			"color": "red"
		},
		"colors": {
			"brand": {
				"dark": "#0186b5",
				"light": "#0186b5"
			},
			"brand-accent": {
				"dark": "#0d7ba6",
				"light": "#0d7ba6"
			},
			"text-on-brand": {
				"dark": "#f7fff2",
				"light": "#f7fff2"
			},
			"background": {
				"dark": "#f4f4f4",
				"light": "#f4f4f4"
			},
			"background-back": {
				"dark": "#d5d2d1",
				"light": "#d5d2d1"
			},
			"background-front": {
				"dark": "#d5d2d1",
				"light": "#d5d2d1"
			},
			"background-contrast": {
				"dark": "#FFFFFF55",
				"light": "#11111111"
			},
			"text": {
				"dark": "#1B1A20",
				"light": "#1B1A20"
			},
			"text-strong": {
				"dark": "#1B1A20",
				"light": "#000000"
			},
			"text-weak": {
				"dark": "#505050",
				"light": "#505050"
			},
			"text-xweak": {
				"dark": "#1B1A20",
				"light": "#1B1A20"
			},
			"border": {
				"dark": "#444444",
				"light": "#CCCCCC"
			},
			"control": "brand",
			"active-background": "background-contrast",
			"active-text": "text-strong",
			"selected-background": "brand",
			"selected-text": "text-strong",
			"status-critical": "#f65d47",
			"status-warning": "#FFAA15",
			"status-ok": "#00C781",
			"status-unknown": "#CCCCCC",
			"status-disabled": "#CCCCCC",
			"graph-0": "brand",
			"graph-1": "status-warning"
		},
		"font": {
			"family": "'Muli', Arial, sans-serif"
		},
		"active": {
			"background": "active-background",
			"color": "active-text"
		},
		"hover": {
			"background": "active-background",
			"color": "active-text"
		},
		"selected": {
			"background": "selected-background",
			"color": "selected-text"
		}
	},
	"chart": {},
	"diagram": {
		"line": {}
	},
	"meter": {}
}