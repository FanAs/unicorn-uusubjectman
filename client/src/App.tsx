import React, {useEffect} from "react"
import './App.css';
import {Provider, useDispatch, useSelector} from "react-redux"
import {BrowserRouter, Route, Switch} from "react-router-dom"
import {fetchUser} from "./features/login/LoginActions"
import {AboutPage} from "./pages/AboutPage"
import {AddLanguagePage} from "./pages/AddLanguagePage"
import {AuditLogPage} from "./pages/AuditLogPage"
import {CreateSubjectPage} from "./pages/CreateSubjectPage"
import {EditLanguagePage} from "./pages/EditLanguagePage"
import {EditSubjectPage} from "./pages/EditSubjectPage"
import {NotFoundPage} from "./pages/NotFoundPage"
import {SubjectPage} from "./pages/SubjectPage"
import {SubjectsPage} from "./pages/SubjectsPage"
import {TranslationPage} from "./pages/TranslationPage"
import {RootState, store} from "./store"

function App() {
  return (
      <>
          <Provider store={store}>
              <ProviderApp />
        </Provider>
      </>
  );
}

function ProviderApp() {
    const dispatch = useDispatch();
    const login = useSelector((state: RootState) => state.login);

    useEffect(() => {
        if (login.data == null && !login.isLoading && !login.isLoaded) {
            dispatch(fetchUser())
        }
    });

    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={SubjectsPage} />
                <Route path="/about" component={AboutPage} />
                <Route path="/auditLog" component={AuditLogPage} />
                <Route path="/subject/create" component={CreateSubjectPage} />
                <Route path="/subject/edit/:id" component={EditSubjectPage} />
                <Route path="/translation/create" component={AddLanguagePage} />
                <Route path="/translation/edit/:id" component={EditLanguagePage} />
                <Route path="/subject/:id/:translationId" component={TranslationPage} />
                <Route path="/subject/:id" component={SubjectPage} />
                <Route path="*" component={NotFoundPage} />
            </Switch>
        </BrowserRouter>
    )
}

export default App;
