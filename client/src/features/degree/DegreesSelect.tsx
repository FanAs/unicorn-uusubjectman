import React, { Component } from "react";

import { Box, TextInput } from "grommet";
import styled from "styled-components"
import {TDegree} from "../../../../src/api/models/TDegree"
import {theme} from "../../styles/theme"

import Degree from "./Degree";

const renderDegree = (degree: TDegree, onRemove?: (degree: TDegree) => void) => {
	return (
		<Box align="center" direction="row" wrap={true} pad={{ left: "xsmall" }}>
			<Degree key={degree.id} onRemove={onRemove == null ? null : () => onRemove(degree)}>
				{degree.name}
			</Degree>
		</Box>
	);
};

const StyledTextInput = styled(TextInput)`
	box-shadow: 0 0 1px 1px ${theme.global.colors.border.light};
	&:focus {
		box-shadow: 0 0 1px 1px ${theme.global.colors.brand.light};
		border: none;
	}
`

export class DegreesSelect extends Component<{ disabled?: boolean, suggestions: TDegree[], value: TDegree | null, onRemove?: (degree: TDegree) => void, onSelect: (degree: TDegree) => void }> {
	static defaultProps = {
		suggestions: [],
		value: []
	};
	state = {
		search: ""
	};
	render() {
		const { suggestions, value, onRemove, onSelect, disabled } = this.props;
		const { search } = this.state;

		return (
			<Box
				wrap={true}
				direction="row"
				align="center"
				round="xsmall"
				pad="xxsmall"
			>
				{value != null && renderDegree(value, onRemove)}
				<Box
					alignSelf="stretch"
					align="start"
					flex={true}
					style={{ minWidth: "240px" }}
				>
					{!disabled && value == null && <StyledTextInput
						plain={true}
						placeholder="Select degree"
						type="search"
						value={search}
						onChange={({ target: { value: search } }) =>
							this.setState({ search })
						}
						onSelect={({ suggestion }) =>
							this.setState(
								{
									search: ""
								},
								() => {
									const degree = suggestions.find((degree) => degree.name === suggestion);
									if (degree != null) {
										onSelect(degree);
									}
								},
							)
						}
						suggestions={suggestions.filter((suggestion) => suggestion.name.toLowerCase().indexOf(search.toLowerCase()) >= 0).map((degree) => degree.name)}
					/>}
				</Box>
			</Box>
		);
	}
}
