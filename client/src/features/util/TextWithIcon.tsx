import {Box, Text} from "grommet"
import {PropsOf} from "grommet/utils"
import React from "react"

export function TextWithIcon(props: PropsOf<typeof Box> & { text: string, icon: any }) {
	return (
		<Box direction={"row"} align={"center"} gap={"xsmall"} {...props}>
			{React.cloneElement(props.icon, {color: props.color})}
			<Text color={props.color} style={{marginLeft: '4px'}} size={"medium"}>{props.text}</Text>
		</Box>
	)
}