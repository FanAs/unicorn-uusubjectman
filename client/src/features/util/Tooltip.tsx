import {Box, BoxTypes} from "grommet"
import React, {FunctionComponent} from "react"
import styled from "styled-components"

const TooltipBox = styled(Box)`
    padding: 8px;
    border-radius: 2px;
    background-color: #4a4a4a;
    color: #d6d6d6;
`

export const Tooltip: FunctionComponent<Omit<BoxTypes, 'children'> & {children: any}> = (props) => {
	return (
		<TooltipBox alignSelf={'center'} {...props as any}>
			{props.children}
		</TooltipBox>
	)
}