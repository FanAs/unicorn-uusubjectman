import {Box, Button, Heading, Layer} from "grommet"
import React from "react"

export function Confirm({show, setShow, onClick, heading}: {heading: string, onClick: () => void, show: boolean, setShow: (value: boolean) => void}) {
	return (
		<Box>
			{show && (
				<Layer
					onEsc={() => setShow(false)}
					onClickOutside={() => setShow(false)}
				>
					<Box pad={'large'}>
						<Heading level={3}>{heading}</Heading>
						<Box direction={'row'} justify={'center'} gap={'medium'}>
							<Button label="cancel" onClick={() => setShow(false)} />
							<Button label="confirm" color={'red'} onClick={() => onClick()} />
						</Box>
					</Box>
				</Layer>
			)}
		</Box>
	);
}
