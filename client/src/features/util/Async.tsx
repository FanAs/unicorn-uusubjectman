import {cloneElement, Component, FunctionComponent} from "react"
import {LoadingState} from "../../data/useAsyncState"
import React from "react";

export const AsyncLoading: FunctionComponent<{}> = ({children}) => null;

export const AsyncDefault: FunctionComponent<{}> = () => null;

export class AsyncResolved<T> extends Component<{ children: (data: T) => any }> {
	render() {
		let {children} = this.props
		return null
	}
}

export const AsyncRejected = ({children}: {children: (error: Error) => any}) => null;

export class Async<T> extends Component<{ state: LoadingState<T> }> {
	render() {
		let {state, children} = this.props
		if (!Array.isArray(children)) {
			return null
		}

		// @ts-ignore
		const loading: any = children.find((child) => child?.type?.name === AsyncLoading.name)
		// @ts-ignore
		const resolved = children.find((child) => child?.type?.name === AsyncResolved.name)
		// @ts-ignore
		const rejected = children.find((child) => child?.type?.name === AsyncRejected.name)
		// @ts-ignore
		const defaultComponent = children.find((child) => child?.type?.name === AsyncDefault.name)

		return (
			<>
				{!state.isLoaded && !state.isLoading && state.error == null && defaultComponent != null && typeof defaultComponent === 'object' && 'props' in defaultComponent && defaultComponent.props.children}
				{state.isLoading && loading != null && loading.props.children}
				{state.isLoaded && resolved != null && typeof resolved === "object" && "props" in resolved && resolved.props.children(state.data)}
				{state.error && rejected != null && typeof rejected === "object" && "props" in rejected && rejected.props.children(state.error)}
			</>
		)
	}
}
