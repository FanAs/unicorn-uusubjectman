import {Box} from "grommet"
import React, {FunctionComponent} from "react"
import styled from "styled-components"

const ErrorBox = styled(Box)`
    padding: 8px;
    border-radius: 6px;
    background-color: darkred;
    color: #d6d6d6;
`

export const ErrorMessage: FunctionComponent<Omit<typeof Box.propTypes, 'children'> & {children: any}> = (props) => {
	return (
		<ErrorBox className={"error-message"} margin={"small"} alignSelf={'center'} {...props as any}>
			{props.children}
		</ErrorBox>
	)
}