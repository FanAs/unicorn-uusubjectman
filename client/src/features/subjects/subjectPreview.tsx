import {request} from "gaxios"
import {Edit, Trash} from "grommet-icons"
import {useSelector} from "react-redux"
import {useHistory} from "react-router-dom"
import styled from "styled-components";
import { Text, Box } from "grommet"
import { TSubject } from "../../../../src/api/models/TSubject";
import {RootState} from "../../store"
import React, {useState} from "react"
import {Confirm} from "../util/Confirm"


const StyledBox = styled(Box)`
    padding: 8px;

    .circle-play {
        display: none;
    }
    
    &:focus {
        box-shadow: none;
    }
`;

const SubjectName = styled(Text)`
    color: black;
    font-size: 16pt;
    
    &:hover {
	    cursor: pointer;
	    position: relative;
    }
`
const Supervisor = styled(Text)`
    color: gray;
    font-size: 12pt;
`

const Title = styled(Box)`
    display: flex;
    align-items: center;
    flex-direction: row;
    column-gap: 8px;
`

const InfoBox = styled(Box)`
    padding: 1px;
`

export function SubjectPreview({ subject, onDelete }: { subject: TSubject, onDelete: () => void }) {
    const history = useHistory();
    const login = useSelector((state: RootState) => state.login);
    const [show, setShow] = useState<boolean>(false);

    const handleDelete = () => {
        request({
            url: `/api/subject/${subject.id}`,
            method: 'DELETE',
            data: {},
        }).then(() => {
            onDelete();
            setShow(false);
        })
    }

    return (
        <StyledBox>
            <Confirm heading={'Are you sure you want to delete subject?'} show={show} setShow={setShow} onClick={() => handleDelete()} />
            <Title>
                {login.data?.role.canCreateSubject && <Edit style={{cursor: 'pointer'}} onClick={() => history.push(`/subject/edit/${subject.id}`)} />}
                {login.data?.role.canCreateSubject && <Trash style={{cursor: 'pointer'}} onClick={() => {
                    setShow(true);
                }
                } />}
                <SubjectName onClick={() => history.push(`/subject/${subject.id}`)}>{subject.name}</SubjectName>
            </Title>
            <InfoBox>
                <Supervisor>{subject.supervisor.firstName} {subject.supervisor.lastName}</Supervisor>
            </InfoBox>
        </StyledBox>
    )
}