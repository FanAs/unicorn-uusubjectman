import {Box, Heading} from "grommet"
import { TSubject } from "../../../../src/api/models/TSubject";
import { SubjectPreview } from "./subjectPreview";
import React from "react";


export function Subjects({data, onDelete}: {data: TSubject[], onDelete: (subject: TSubject) => void}) {
    const byStudyProgram: Record<string, { degree:TSubject['degree'], subjects: TSubject[] }> = Object.create(null);
    data.forEach((subject) => {
        if (!(subject.degree.id in byStudyProgram)) {
            byStudyProgram[subject.degree.id] = {
                degree: subject.degree,
                subjects: [],
            };
        }

        byStudyProgram[subject.degree.id].subjects.push(subject);
    });

    if (data.length === 0) {
        return <Box><Heading>No subjects available</Heading></Box>
    }

    return(
        <Box>
            {Object.values(byStudyProgram).map((studyProgram, i) =>
                <Box key={i.toString()}>
                    <Heading level={3}>{studyProgram.degree.name}</Heading>
                    <Box>{studyProgram.subjects.map((subject, i) => <SubjectPreview onDelete={() => onDelete(subject)} subject={subject} key={i.toString()} />)}</Box>
                </Box>
            )}
        </Box>
    )
}