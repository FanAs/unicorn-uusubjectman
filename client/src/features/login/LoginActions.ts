import {request} from "gaxios"
import {AppDispatch, AppThunk} from "../../store"
import {loginActions} from "./loginSlice"

export const fetchUser = (): AppThunk => async (dispatch: AppDispatch) => {
	dispatch(loginActions.fetchUserStart());

	const res = await request<any>({
		url: '/api/user/info'
	});

	dispatch(loginActions.fetchUserSuccess(res.data));
}

export const handleLogin = (googleData: any): AppThunk => async (dispatch: AppDispatch) => {
	console.log(googleData);
	dispatch(loginActions.handleLoginStart());

	try {
		const res = await request({
			url: "/auth/google",
			method: "POST",
			data: {
				token: googleData.tokenId
			},
		});

		dispatch(loginActions.handleLoginSuccess(res.data));
	} catch (err) {
		dispatch(loginActions.fetchUserFailure(err));
	}
}

export const handleLogout = (): AppThunk => async (dispatch: AppDispatch) => {
	try {
		const res = await request({
			url: "/auth/logout",
			method: "POST",
		});

		dispatch(loginActions.handleLogoutSuccess(res));
	} catch (err) {
		dispatch(loginActions.handleLogoutError(err));
	}
}