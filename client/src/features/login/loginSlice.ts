import {createSlice} from "@reduxjs/toolkit"
import {TUser} from "../../../../src/api/models/TUser"

export const initialState = {
	isLoading: false,
	isLoaded: false,
	data: <TUser | null>null,
	error: <any>null
};

const slice = createSlice({
	name: 'login',
	initialState,
	reducers: {
		handleLoginSuccess: (
			(state, action) => {
				state.isLoading = false;
				state.isLoaded = true;
				state.data = action.payload.user;
			}
		),
		handleLoginStart: (
			(state) => {
				state.isLoading = true;
			}
		),
		fetchUserSuccess: (
			(state, action) => {
				state.isLoading = false;
				state.isLoaded = true;
				state.data = action.payload;
			}
		),
		fetchUserFailure: (
			(state, action) => {
				state.isLoading = false;
				state.error = action.payload;
			}
		),
		fetchUserStart: (
			(state) => {
				state.isLoading = true;
			}
		),
		handleLogoutSuccess: (state, action) => {
			state.isLoading = false;
			state.isLoaded = false;
			state.data = null;
		},
		handleLogoutError: (state, action) => {
			state.error = action.payload;
		},
	}
})

export const loginActions = slice.actions;
export const loginReducer = slice.reducer;
