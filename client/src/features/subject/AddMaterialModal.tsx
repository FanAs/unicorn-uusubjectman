import {request} from "gaxios"
import {Box, Button, Form, FormField, Heading, Layer, Select, Text, TextInput} from "grommet"
import {Close, Send} from "grommet-icons"
import React, {useState} from "react"
import styled from "styled-components"
import {TLanguage} from "../../../../src/api/models/TLanguage"
import {TMaterial} from "../../../../src/api/models/TMaterial"
import {theme} from "../../styles/theme"
import {Async} from "../util/Async"

const StyledButton = styled(Button)`
	background-color: ${theme.global.colors.brand.light};
	color: ${theme.global.colors['text-on-brand'].light};
	border-radius: 2px;
`

export const AddMaterialModal = (props: {value?: TMaterial, onSubmit: (value: Omit<TMaterial, 'id'>) => void, setShow: (value: boolean) => void}) => {
	const [value, setValue] = useState(props.value ?? {
		name: '',
		url: '',
		type: 'url',
	});


	const onFormChanged = (nextValue: any) => {
		setValue(nextValue);
	}

	const getMessage = () => {
		return undefined;
	}

	const message = getMessage();

	return (
		<Layer onEsc={() => props.setShow(false)} onClickOutside={() => props.setShow(false)}>
			<Box pad={'large'}>
				<Form value={value} onChange={(value) => onFormChanged(value)} onSubmit={({value}) => props.onSubmit(value)}>
					<Box gridArea="header" align="center">
						<Heading size="small">Add material</Heading>
					</Box>
					<Box gridArea="name">
						<FormField name={"name"}>
							<TextInput name={"name"} placeholder="Name" />
						</FormField>
					</Box>
					<Box gridArea="url">
						<FormField name={"url"}>
							<TextInput name={"url"} placeholder="URL" />
						</FormField>
					</Box>
					<Box gridArea="type">
						<FormField name={"type"}>
							<Select name={"type"} value={value.type} options={['video', 'uubook', 'uucourse', 'url']} />
						</FormField>
					</Box>
					<Box gridArea="submit" pad={{vertical: 'xlarge'}}>
						{message && (
							<Box pad={'small'}>
								<Text color="status-critical">{message}</Text>
							</Box>
						)}
						<StyledButton disabled={message !== undefined} type="submit" primary label="submit" icon={<Send color={'text-on-brand'} />} />
						<StyledButton margin={{top: 'small'}} icon={<Close color={'text-on-brand'} />} label="close" onClick={() => props.setShow(false)} />
					</Box>
				</Form>
			</Box>
		</Layer>
	)
}