import React from "react"
import styled from "styled-components"
import {theme} from "../../styles/theme"

const StyledLink = styled.a`
	color: ${theme.global.colors["brand"].dark};
	
	&:hover {
		span {
			text-decoration: underline;
		}
	}
`

export const LinkRenderer: any = (props: { href: string, children: React.ElementType }) => {
	return (
		<StyledLink href={props.href} target="_blank">
			<span>{props.children}</span> {props.href.match(/^(https?:)?\/\//) ? <sup>↗️</sup> : ''}
		</StyledLink>
	)
}
