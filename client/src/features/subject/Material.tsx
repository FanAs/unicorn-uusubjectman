import {Box, Text} from "grommet"
import {Book, CircleQuestion, Link, Video, Workshop} from "grommet-icons"
import styled from "styled-components"
import {TMaterial} from "../../../../src/api/models/TMaterial"
import {TextWithIcon} from "../util/TextWithIcon"
import React from "react"


const StyledBox = styled(Box)`
	background-color: #ffffff;
	color: #252423;
	border-radius: 8px;
    font-size: 12px;
    
    &:focus {
        box-shadow: none;
    }
    
    &:hover {
	    cursor: pointer;
	    position: relative;
    }
`

const TitleText = styled(Text)`
    overflow: hidden;
    line-height: 1.4em;
    height: 2.8em;
`

const IconByType = ({type}: {type: TMaterial['type']}) => {
	switch (type) {
		case 'video':
			return <Video />;
		case 'uubook':
			return <Book />;
		case 'uucourse':
			return <Workshop />;
		case 'url':
			return <Link />;
		default:
			return <CircleQuestion />;
	}
}

export const Material = ({value, onClick}: {value: TMaterial | Omit<TMaterial, 'id'>, onClick?: (value: TMaterial | Omit<TMaterial, 'id'>) => void}) => {
	return (
		<StyledBox pad="small" margin="small">
			<TitleText onClick={() => onClick != null && onClick(value)}><TextWithIcon text={value.name} icon={<IconByType type={value.type} />} /></TitleText>
		</StyledBox>
	)
}