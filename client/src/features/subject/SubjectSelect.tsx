import React, { Component } from "react";

import { Box, TextInput } from "grommet";
import styled from "styled-components"
import {TSubject} from "../../../../src/api/models/TSubject"
import {theme} from "../../styles/theme"

import Subject from "./Subject";

const renderSubject = (subject: TSubject, onRemove?: (subject: TSubject) => void) => {
	return (
		<Box align="center" direction="row" wrap={true} pad={{ left: "xsmall" }}>
			<Subject key={subject.id} onRemove={onRemove == null ? null : () => onRemove(subject)}>
				{subject.name}
			</Subject>
		</Box>
	);
};

const StyledTextInput = styled(TextInput)`
	box-shadow: 0 0 1px 1px ${theme.global.colors.border.light};
	&:focus {
		box-shadow: 0 0 1px 1px ${theme.global.colors.brand.light};
		border: none;
	}
`

export class SubjectsSelect extends Component<{ disabled?: boolean, suggestions: TSubject[], value: TSubject | null, onRemove?: (subject: TSubject) => void, onSelect: (subject: TSubject) => void }> {
	static defaultProps = {
		suggestions: [],
		value: []
	};
	state = {
		search: ""
	};
	render() {
		const { suggestions, value, onRemove, onSelect, disabled } = this.props;
		const { search } = this.state;

		return (
			<Box
				wrap={true}
				direction="row"
				align="center"
				round="xsmall"
				pad="xxsmall"
			>
				{value != null && renderSubject(value, onRemove)}
				<Box
					alignSelf="stretch"
					align="start"
					flex={true}
					style={{ minWidth: "240px" }}
				>
					{!disabled && value == null && <StyledTextInput
						plain={true}
						placeholder="Select subject"
						type="search"
						value={search}
						onChange={({ target: { value: search } }) =>
							this.setState({ search })
						}
						onSelect={({ suggestion }) =>
							this.setState(
								{
									search: ""
								},
								() => {
									const subject = suggestions.find((subject) => subject.name === suggestion);
									if (subject != null) {
										onSelect(subject);
									}
								},
							)
						}
						suggestions={suggestions.filter((suggestion) => suggestion.name.toLowerCase().indexOf(search.toLowerCase()) >= 0).map((subject) => subject.name)}
					/>}
				</Box>
			</Box>
		);
	}
}
