import React from "react";

import { Box, Button, Text } from "grommet";

import { FormClose } from "grommet-icons";
import styled from "styled-components"

const StyledText = styled(Text)`
	min-width: 48px;
	text-align: center;
`

const StyledBox = styled(Box)`
	border-radius: 2px;
`

const renderSubject = (children: any, onRemove: any) => (
	<StyledBox
		background="brand"
		direction="row"
		align="center"
		round="xsmall"
		pad="xsmall"
		gap="xsmall"
		margin="xxsmall"
	>
		<StyledText size="small" color="text-on-brand">{children}</StyledText>
		{onRemove && (
			<Box background={{ color: "white", opacity: "strong" }} round="full">
				<FormClose color='brand' style={{ width: "12px", height: "12px" }} />
			</Box>
		)}
	</StyledBox>
);

const Subject = ({ children, onRemove }: any) =>
	onRemove ? (
		<Button onClick={onRemove}>{renderSubject(children, onRemove)}</Button>
	) : (
		renderSubject(children, onRemove)
	);

export default Subject;
