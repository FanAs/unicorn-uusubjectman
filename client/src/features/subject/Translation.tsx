import React, {useState} from "react"
import styled from "styled-components";
import {Text, Box, Grid, Button} from "grommet"
import {TMaterial} from "../../../../src/api/models/TMaterial"
import {TSubjectTranslation} from "../../../../src/api/models/TSubjectTranslation"
import {dictionary} from "../../data/dictionary"
import {Description} from "./Description"
import {LinkRenderer} from "./LinkRenderer"
import {Material} from "./Material"
import {MaterialModal} from "./MaterialModal"


const StyledBox = styled(Box)`
    padding: 8px;

    .circle-play {
        display: none;
    }
    
    &:focus {
        box-shadow: none;
    }
`;

const SubjectName = styled(Text)`
    color: black;
    font-size: 20pt;

    &:hover {
	    cursor: pointer;
	    position: relative;
    }
`
const Supervisor = styled(Text)`
    color: gray;
    font-size: 12pt;
`
const Credits = styled(Text)`
    color: gray;
    font-size: 12pt;
`

const Goal = styled(Text)`
    color: black;
    font-size: 14pt;
    font-weight: bold;
`

const InfoBox = styled(Box)`
    padding: 1px;
`

export function Translation({ translation }: { translation: TSubjectTranslation }) {
    const [value, setValue] = useState<TMaterial | null>(null);

    return (
        <StyledBox margin={{top: 'medium', left: 'small'}}>
            {value != null && <MaterialModal value={value} setShow={(value) => {
                if (!value) {
                    setValue(null);
                }
            }} />}
            <SubjectName margin={{left: 'xsmall'}}>{translation.name}</SubjectName>
            <InfoBox margin={{left: 'xsmall'}}>
                <Supervisor>{translation.subject.supervisor.firstName} {translation.subject.supervisor.lastName}</Supervisor>
                <Credits>{translation.subject.credits} {dictionary[translation.language.name].creditsMultiple}</Credits>
                <Goal margin={{top: 'small'}}>The goal of a subject: {translation.goal}</Goal>
            </InfoBox>
            <Description components={{a: LinkRenderer, iframe: () => 'iframe is not supported'}}>{translation.description}</Description>
            <Grid columns="medium">
                {translation.materials.map((material) => <Material onClick={() => setValue(material)} value={material} />)}
            </Grid>
        </StyledBox>
    )
}