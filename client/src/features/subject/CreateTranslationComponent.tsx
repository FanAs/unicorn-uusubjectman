import MDEditor from "@uiw/react-md-editor"
import {request} from "gaxios"
import {Box, Button, Form, FormField, Heading, TextInput, Text, Grid, Notification} from "grommet"
import {AddCircle, Send} from "grommet-icons"
import React, {useState} from "react"
import {useHistory} from "react-router-dom"
import styled from "styled-components"
import {TLanguage} from "../../../../src/api/models/TLanguage"
import {TMaterial} from "../../../../src/api/models/TMaterial"
import {TSubject} from "../../../../src/api/models/TSubject"
import {TSubjectTranslation} from "../../../../src/api/models/TSubjectTranslation"
import {ApiPostTranslationType} from "../../../../src/api/routes/queryValidators/apiPostTranslationValidator"
import {useAsyncState} from "../../data/useAsyncState"
import {theme} from "../../styles/theme"
import { LanguagesSelect } from "../language/LanguagesSelect"
import {Async, AsyncLoading, AsyncRejected, AsyncResolved} from "../util/Async"
import {ErrorMessage} from "../util/Error"
import {Spinner} from "../util/Spinner"
import {TextWithIcon} from "../util/TextWithIcon"
import {AddMaterialModal} from "./AddMaterialModal"
import {Material} from "./Material"
import {SubjectsSelect} from "./SubjectSelect"

const CustomMDEditor = styled(MDEditor)`
	.w-md-editor-bar {
		display: none;
	}
	
	.w-md-editor-preview {
	    white-space: pre-wrap;
	}
`

const StyledButton = styled(Button)`
	background-color: ${theme.global.colors.brand.light};
	color: ${theme.global.colors['text-on-brand'].light};
	border-radius: 2px;
`

export function CreateTranslationComponent(props: {value?: TSubjectTranslation, onSubmit?: (language: TLanguage, id: number) => void, id?: number}) {
	const [isUploading, setIsUploading] = useState(false);
	const [show, setShow] = useState(false);
	const [materials, setMaterials] = useState<(TMaterial | Omit<TMaterial, 'id'>)[]>(props.value?.materials ?? []);
	const [description, setDescription] = useState(props.value?.description ?? '');
	const [selectedLanguage, setSelectedLanguage] = useState(props.value?.language ?? null);
	const [selectedSubject, setSelectedSubject] = useState(props.value?.subject ?? null);
	const [languages] = useAsyncState<TLanguage[]>([], async () => {
		const {data} = await request<TLanguage[]>({
			url: '/api/languages',
			method: 'GET',
		});

		return data;
	});

	const [subjects] = useAsyncState<TSubject[]>([], async () => {
		const {data} = await request<TSubject[]>({
			url: '/api/subjects',
			method: 'GET',
		});

		return data;
	});

	const [value, setValue] = useState<{name: string, goal: string}>(props.value ?? {
		name: '',
		goal: '',
	});

	const getMessage = () => {
		if (description.length > 65536) {
			return 'Description is too long - max length is 65536 characters';
		}

		if (description.length === 0) {
			return 'Must enter description';
		}

		if (selectedLanguage == null) {
			return 'Must select language';
		}

		if (selectedSubject == null) {
			return 'Must select subject';
		}

		if (value.name.length === 0) {
			return 'Must enter name';
		}

		if (value.goal.length === 0) {
			return 'Must enter goal';
		}

		return undefined;
	}

	const [error, setError] = useState<string | null>(null);

	const message = getMessage();

	const onFormChanged = (nextValue: any) => {
		setValue(nextValue);
	}

	const handleSubmitForm = ({value}: any) => {
		setIsUploading(true);
		const body: ApiPostTranslationType = {
			name: value.name,
			goal: value.goal,
			description: description,
			subjectId: selectedSubject!.id,
			languageId: selectedLanguage!.id,
			materials: materials,
		};

		request<{id: number}>({
			url: props.value == null ? '/api/translation' : `/api/translation/${props.value?.id}`,
			method: props.value == null ? 'POST' : 'PATCH',
			data: body,
		}).then((res) => {
			setIsUploading(false);
			if (props.onSubmit != null) {
				props.onSubmit(selectedLanguage as TLanguage, res.data.id);
			}
		}).catch((err) => {
			setIsUploading(false);
			setError(err.response?.data?.message ?? err.message);
		})
	}

	return (
		<>
			{show && <AddMaterialModal onSubmit={(value) => {
				setMaterials([...materials, value]);
				setShow(false);
			}} setShow={(value) => setShow(value)} />}
			<Form value={value} onChange={(value) => onFormChanged(value)} onSubmit={(value) => handleSubmitForm(value)}>
				<Box>
					{error != null && (
						<Notification
							toast
							title="Error"
							message={error}
							status="warning"
							onClose={() => setError(null)}
						/>
					)}
					<Box gridArea="header" align="center">
						<Heading size="small">Add content to a subject</Heading>
					</Box>
					<Async state={languages}>
						<AsyncLoading><Spinner /></AsyncLoading>
						<AsyncResolved>
							{(data: typeof languages.data) =>
								<Box gridArea="languages" pad={{vertical: 'xsmall'}}>
									<LanguagesSelect
										value={selectedLanguage}
										suggestions={data.sort().filter((suggestion: TLanguage) => suggestion.id !== selectedLanguage?.id)}
										onSelect={(language) => {
											setSelectedLanguage(language);
										}}
										onRemove={() => {
											setSelectedLanguage(null)
										}}
									/>
								</Box>
							}
						</AsyncResolved>
						<AsyncRejected>{(err: any) => <ErrorMessage>{err.message}</ErrorMessage>}</AsyncRejected>
					</Async>
					<Async state={subjects}>
						<AsyncLoading><Spinner /></AsyncLoading>
						<AsyncResolved>
							{(data: typeof subjects.data) =>
								<Box gridArea="subjects" pad={{vertical: 'xsmall'}}>
									<SubjectsSelect
										value={selectedSubject}
										suggestions={data.sort().filter((suggestion: TSubject) => suggestion.id !== selectedSubject?.id)}
										onSelect={(subject) => {
											setSelectedSubject(subject);
										}}
										onRemove={() => {
											setSelectedSubject(null)
										}}
									/>
								</Box>
							}
						</AsyncResolved>
						<AsyncRejected>{(err: any) => <ErrorMessage>{err.message}</ErrorMessage>}</AsyncRejected>
					</Async>
					<Box gridArea="name">
						<FormField name={"name"}>
							<TextInput name={"name"} placeholder="Name of a subject in local language" />
						</FormField>
					</Box>
					<Box gridArea="goal">
						<FormField name={"goal"}>
							<TextInput name={"goal"} placeholder="Goal of a subject" />
						</FormField>
					</Box>
					<Box gridArea="description" style={{height: '100%'}}>
						<CustomMDEditor
							value={description}
							onChange={(val) => {
								setDescription(val as string);
							}}
							preview='edit'
						/>
					</Box>
					<Box gridArea="materials" pad={{vertical: 'medium'}}>
						<Grid columns="medium">
							{materials.map((material) => <Material value={material} />)}
							<Box pad="small" margin="small">
								<Button><TextWithIcon text="Add material" icon={<AddCircle />} onClick={() => setShow(true)}/></Button>
							</Box>
						</Grid>
					</Box>
					<Box gridArea="submit" pad={{vertical: 'xlarge'}}>
						{message && (
							<Box pad={'small'}>
								<Text color="status-critical">{message}</Text>
							</Box>
						)}
						<StyledButton disabled={message !== undefined || isUploading} type="submit" primary label="submit" icon={<Send color={'text-on-brand'} />} />
						{isUploading && <Spinner message={"Uploading..."} />}
					</Box>
				</Box>
			</Form>
		</>
	)
}