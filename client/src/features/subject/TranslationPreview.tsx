import {request} from "gaxios"
import {Edit, Trash} from "grommet-icons"
import {useSelector} from "react-redux"
import {useHistory} from "react-router-dom"
import styled from "styled-components";
import {Text, Box, Heading, Select, Grid} from "grommet"
import {TLanguage} from "../../../../src/api/models/TLanguage"
import { TSubject } from "../../../../src/api/models/TSubject";
import {TSubjectTranslation} from "../../../../src/api/models/TSubjectTranslation"
import {useAsyncState} from "../../data/useAsyncState"
import {Page} from "../../pages/Page"
import {RootState} from "../../store"
import React, {useState} from "react"
import {Async, AsyncLoading, AsyncRejected, AsyncResolved} from "../util/Async"
import {Confirm} from "../util/Confirm"
import {ErrorMessage} from "../util/Error"
import {Spinner} from "../util/Spinner"


const StyledBox = styled(Box)`
    padding: 8px;

    .circle-play {
        display: none;
    }
    
    &:focus {
        box-shadow: none;
    }
`;

const SubjectName = styled(Text)`
    color: black;
    font-size: 16pt;
    
    &:hover {
	    cursor: pointer;
	    position: relative;
    }
`
const Supervisor = styled(Text)`
    color: gray;
    font-size: 12pt;
`

const InfoBox = styled(Box)`
    padding: 1px;
`

const Title = styled(Box)`
    display: flex;
    align-items: center;
    flex-direction: row;
    column-gap: 8px;
`

export function TranslationPreview({ translations, onDelete }: { translations: TSubjectTranslation[], onDelete: (translation: TSubjectTranslation) => void }) {
	const history = useHistory();
	const login = useSelector((state: RootState) => state.login);
	const [toDelete, setToDelete] = useState<TSubjectTranslation | null>(null);

	const [languages] = useAsyncState<TLanguage[]>([], async () => {
		const {data} = await request<TLanguage[]>({
			url: '/api/languages',
			method: 'GET',
		});

		setSelectedLanguage(data[0]);
		return data;
	});

	const [selectedLanguage, setSelectedLanguage] = useState<TLanguage | null>();

	const handleDelete = () => {
		if (toDelete == null) {
			return;
		}

		request({
			url: `/api/translation/${toDelete.id}`,
			method: 'DELETE',
			data: {},
		}).then(() => {
			onDelete(toDelete);
			setToDelete(null);
		})
	}

	const filteredTranslations = translations.filter((translation) => selectedLanguage == null || translation.language.id === selectedLanguage.id);

	return (
		<StyledBox>
			<Confirm heading={'Are you sure you want to delete translation?'} show={toDelete != null} setShow={() => setToDelete(null)} onClick={() => handleDelete()} />
			<Grid rows={['xsmall']} columns={['50%', '50%']} areas={[{name: 'a', start: [0,0], end: [0,0]}, {name: 'b', start: [1,0], end: [1,0]}]}>

				<Heading gridArea={'a'} size={'small'}>
					{filteredTranslations.length === 0 ? 'No content available' : 'Subject content'}
				</Heading>
				<Async state={languages}>
					<AsyncLoading><Spinner /></AsyncLoading>
					<AsyncResolved>
						{(data: typeof languages.data) =>
							<Box gridArea="b" pad='medium'>
								<Select
									options={data}
									valueKey={'id'}
									labelKey={'localName'}
									multiple={false}
									value={selectedLanguage ?? data[0]}
									alignSelf={"end"}
									onChange={({option}) => setSelectedLanguage(option)}
								/>
							</Box>
						}
					</AsyncResolved>
					<AsyncRejected>{(err: any) => <ErrorMessage>{err.message}</ErrorMessage>}</AsyncRejected>
				</Async>
			</Grid>
				{filteredTranslations.map((translation) =>
				<StyledBox>
					<Title>
						{login.data?.role.canSuperviseSubject && <Edit style={{cursor: 'pointer'}} onClick={() => history.push(`/translation/edit/${translation.id}`)} />}
						{login.data?.role.canSuperviseSubject && <Trash style={{cursor: 'pointer'}} onClick={() => {
							setToDelete(translation);
						}
						} />}
						<SubjectName onClick={() => history.push(`/subject/${translation.subject.id}/${translation.id}`)}>{translation.name} - {translation.language.localName}</SubjectName>
					</Title>
					<InfoBox>
						<Supervisor>{translation.subject.supervisor.firstName} {translation.subject.supervisor.lastName}</Supervisor>
					</InfoBox>
				</StyledBox>
			)}
		</StyledBox>
	)
}