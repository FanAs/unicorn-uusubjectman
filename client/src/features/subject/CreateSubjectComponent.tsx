import MDEditor from "@uiw/react-md-editor"
import {request} from "gaxios"
import {Box, Button, Form, FormField, Heading, TextInput, Text, Notification, CheckBox} from "grommet"
import { Send } from "grommet-icons"
import React, {useState} from "react"
import styled from "styled-components"
import {TDegree} from "../../../../src/api/models/TDegree"
import {TSubject} from "../../../../src/api/models/TSubject"
import {TUser} from "../../../../src/api/models/TUser"
import {useAsyncState} from "../../data/useAsyncState"
import {theme} from "../../styles/theme"
import {DegreesSelect} from "../degree/DegreesSelect"
import {SupervisorSelect} from "../supervisor/SupervisorSelect"
import {Async, AsyncLoading, AsyncRejected, AsyncResolved} from "../util/Async"
import {ErrorMessage} from "../util/Error"
import {Spinner} from "../util/Spinner"

const CustomMDEditor = styled(MDEditor)`
	.w-md-editor-bar {
		display: none;
	}
	
	.w-md-editor-preview {
	    white-space: pre-wrap;
	}
`

const StyledButton = styled(Button)`
	background-color: ${theme.global.colors.brand.light};
	color: ${theme.global.colors['text-on-brand'].light};
	border-radius: 2px;
`

export function CreateSubjectComponent(props: {value?: TSubject, onSubmit?: (id: number) => void, id?: number}) {
	const [selectedDegree, setSelectedDegree] = useState<TDegree | null>(props.value?.degree ?? null);

	const [degrees] = useAsyncState<TDegree[]>([], async () => {
		const {data} = await request<TDegree[]>({
			url: '/api/degrees',
			method: 'GET',
		});

		return data;
	});

	const [selectedSupervisor, setSelectedSupervisor] = useState<TUser | null>(props.value?.supervisor ?? null);
	const [supervisors] = useAsyncState<TUser[]>([], async () => {
		const {data} = await request<TUser[]>({
			url: '/api/supervisors',
			method: 'GET',
		});

		return data;
	});

	const [value, setValue] = useState(props.value ?? {
		name: '',
		credits: 0,
	});

	const [isUploading, setIsUploading] = useState(false);

	const [error, setError] = useState<string | null>(null);

	const getMessage = () => {
		if (value.name.length === 0) {
			return 'You need to fill in name';
		}

		if (value.credits <= 0) {
			return 'Must enter correct value for credits';
		}

		if (selectedSupervisor == null) {
			return 'Must select supervisor';
		}

		// selectedDegree osetreni neni naschval abychom overili ze to funguje spravne

		return undefined;
	}

	const message = getMessage();

	const onFormChanged = (nextValue: any) => {
		setValue(nextValue);
	}

	const handleSubmitForm = ({value}: any) => {
		setIsUploading(true);
		request<{id: number}>({
			url: props.value?.id == null ? '/api/subject' : `/api/subject/${props.value?.id}`,
			method: props.value?.id == null ? 'POST' : 'PATCH',
			data: {
				credits: Number(value.credits),
				name: value.name,
				degreeId: selectedDegree?.id,
				supervisorId: selectedSupervisor?.id,
				isRequired: value.isRequired ?? false,
			},
		}).then((res) => {
			setIsUploading(false);
			if (props.onSubmit != null) {
				props.onSubmit(res.data.id);
			}
		}).catch((err) => {
			setError(err.response?.data?.message ?? err.message);
			setIsUploading(false);
		})
	}

	return (
		<Form value={value} onChange={(value) => onFormChanged(value)} onSubmit={(value) => handleSubmitForm(value)}>
			<Box>
				{error != null && (
					<Notification
						toast
						title="Error"
						message={error}
						status="warning"
						onClose={() => setError(null)}
					/>
				)}
				<Box gridArea="header" align="center">
					<Heading size="small">{props.value == null ? 'Create new subject' : 'Edit subject'}</Heading>
				</Box>
				<Box gridArea="name">
					<FormField name={"name"}>
						<TextInput name={"name"} placeholder="Internal name for a subject" />
					</FormField>
				</Box>
				<Box gridArea="credits">
					<FormField name={"credits"}>
						<TextInput type="number" name={"credits"} placeholder="Credits" />
					</FormField>
				</Box>
				<Box gridArea="isRequired">
					<FormField name={"isRequired"}>
						<CheckBox name={"isRequired"} label="Required subject" />
					</FormField>
				</Box>
				<Async state={degrees}>
					<AsyncLoading><Spinner /></AsyncLoading>
					<AsyncResolved>
						{(data: typeof degrees.data) =>
							<Box gridArea="degree" pad={{vertical: 'xsmall'}}>
								<DegreesSelect
									value={selectedDegree}
									suggestions={data.sort().filter((suggestion: TDegree) => suggestion.id !== selectedDegree?.id)}
									onSelect={(degree: TDegree) => {
										setSelectedDegree(degree);
									}}
									onRemove={() => {
										setSelectedDegree(null)
									}}
								/>
							</Box>
						}
					</AsyncResolved>
					<AsyncRejected>{(err: any) => <ErrorMessage>{err.message}</ErrorMessage>}</AsyncRejected>
				</Async>
				<Async state={supervisors}>
					<AsyncLoading><Spinner /></AsyncLoading>
					<AsyncResolved>
						{(data: typeof supervisors.data) =>
							<Box gridArea="supervisor" pad={{vertical: 'xsmall'}}>
								<SupervisorSelect
									value={selectedSupervisor}
									suggestions={data.sort().filter((supervisor: TUser) => supervisor.id !== selectedSupervisor?.id)}
									onSelect={(supervisor: TUser) => {
										setSelectedSupervisor(supervisor);
									}}
									onRemove={() => {
										setSelectedSupervisor(null)
									}}
								/>
							</Box>
						}
					</AsyncResolved>
					<AsyncRejected>{(err: any) => <ErrorMessage>{err.message}</ErrorMessage>}</AsyncRejected>
				</Async>
				<Box gridArea="submit" pad={{vertical: 'xlarge'}}>
					{message && (
						<Box pad={'small'}>
							<Text color="status-critical">{message}</Text>
						</Box>
					)}
					<StyledButton disabled={message !== undefined || isUploading} type="submit" primary label="submit" icon={<Send color={'text-on-brand'} />} />
					{isUploading && <Spinner message={"Uploading..."} />}
				</Box>
			</Box>
		</Form>
	)
}