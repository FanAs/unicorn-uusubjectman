import ReactMarkdown from "react-markdown"
import styled from "styled-components"

export const Description = styled(ReactMarkdown)`
    white-space: pre-wrap;
    margin-left: 8px;
`
