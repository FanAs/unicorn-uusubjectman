import {Box, Button, Form, FormField, Heading, Layer, Select, Text, TextInput} from "grommet"
import {Close, Send, View} from "grommet-icons"
import React, {useState} from "react"
import styled from "styled-components"
import {TLanguage} from "../../../../src/api/models/TLanguage"
import {TMaterial} from "../../../../src/api/models/TMaterial"
import {theme} from "../../styles/theme"
import {Async} from "../util/Async"

const StyledButton = styled(Button)`
	background-color: ${theme.global.colors.brand.light};
	color: ${theme.global.colors['text-on-brand'].light};
	border-radius: 2px;
`

export const MaterialModal = (props: {value: TMaterial | null, setShow: (value: boolean) => void}) => {
	if (props.value == null) {
		return null;
	}

	return (
		<Layer onEsc={() => props.setShow(false)} onClickOutside={() => props.setShow(false)}>
			<Box pad={'large'}>
				<Box gridArea="header" align="center">
					<Heading size="small">{props.value.name}</Heading>
				</Box>
				<StyledButton margin={{top: 'small'}} icon={<View color={'text-on-brand'} />} label={`Open ${props.value.type}`} onClick={() => window.open(props.value?.url, "_blank")} />
				<StyledButton margin={{top: 'small'}} icon={<Close color={'text-on-brand'} />} label="close" onClick={() => props.setShow(false)} />
			</Box>
		</Layer>
	)
}