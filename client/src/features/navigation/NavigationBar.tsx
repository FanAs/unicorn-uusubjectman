import {request} from "gaxios"
import {Box, Button, Grommet, Header, Menu, Text, TextInput} from "grommet"
import {FormDown, Home, Search, User} from "grommet-icons"
import React, {useMemo, useState} from "react"
import {GoogleLogin} from "react-google-login"
import {useDispatch, useSelector} from "react-redux"
import styled from "styled-components"
import {TLanguage} from "../../../../src/api/models/TLanguage"
import {TUser} from "../../../../src/api/models/TUser"
import {RootState} from "../../store"
import {theme} from "../../styles/theme"
import {handleLogin} from "../login/LoginActions"
import {loginActions} from "../login/loginSlice"

const CustomHeader = styled(Header)`
	box-shadow: none;
	color: ${theme.global.colors.background.dark};
`

const ResponsiveBox = styled(Box)`
	font-family: 'Bungee';
	display: none;
	@media screen and (min-width: 800px) {
		display: flex;
	}
`

const ResponsiveText = styled(Text)`
	display: none;
	@media screen and (min-width: 800px) {
		display: flex;
	}
`

const getNextRole = (user: TUser | null) => {
	if (user == null) {
		return null;
	}

	switch (user.role.id) {
		case 2:
			return { id: 1, name: 'administrator' }
		case 1:
			return { id: 2, name: 'normal user' };
		default:
			throw new Error('Role is not supported');
	}
}

export function NavigationBar() {
	const dispatch = useDispatch();
	const login = useSelector((state: RootState) => state.login);
	const nextRole = getNextRole(login.data);

	return (
		<CustomHeader background="brand" elevation='small' flex={false}>
			<ResponsiveBox pad={"small"}><Text size={"large"}>uuSubjectMan</Text></ResponsiveBox>
			<Box pad="xxsmall" direction="row" align={"center"} gap="small">
				<Box direction="row" gap="small" pad="xsmall">
					<Box direction={"row"} gap={"xsmall"}>
						{nextRole &&
						<Box style={{cursor: 'pointer'}} onClick={() => {
							if (nextRole == null) {
								return;
							}

							request<TUser>({
								url: '/api/user/setRole',
								method: 'POST',
								data: {
									roleId: nextRole.id,
								},
							}).then((res) => {
								dispatch(loginActions.fetchUserSuccess(res.data));
							})
						}}>
							Set role to {nextRole.name}
						</Box>}
						<User />
						{login.data == null ?
							<GoogleLogin
								prompt="consent"
								clientId="1074640251162-e48fj1pjubq8e91fg34ugccifg4o1rml.apps.googleusercontent.com"
								render={(props) => (<Text style={{cursor: 'pointer'}} onClick={props.onClick} size={"medium"}>log in</Text>)}
								onSuccess={(data) => dispatch(handleLogin(data))}
								onFailure={(data) => dispatch(handleLogin(data))}
								cookiePolicy={'single_host_origin'}
							/> : <ResponsiveText size={"medium"}>{login.data!.firstName} {login.data!.lastName}</ResponsiveText>}
					</Box>
				</Box>
			</Box>
		</CustomHeader>
	)
}
