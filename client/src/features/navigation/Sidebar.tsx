import {Box, Button, Sidebar as GrommetSidebar} from "grommet"
import {Add, CircleQuestion, Home, Logout, ShieldSecurity} from "grommet-icons"
import {useDispatch, useSelector} from "react-redux"
import styled from "styled-components"
import {RootState} from "../../store"
import {theme} from "../../styles/theme"
import {handleLogout} from "../login/LoginActions"
import {TextWithIcon} from "../util/TextWithIcon"
import {Link, useLocation} from 'react-router-dom';
import React from "react"

const GrommetCustomSidebar = styled(GrommetSidebar)`
	min-width: 60px;
	@media screen and (min-width: 800px) {
		min-width: 220px;
	}
	min-height: 100vh;
	position: fixed;
	font-family: 'Bungee';
`

const StyledButton = styled(Button)`
	padding: 16px;
	border-radius: 0 !important;
	color: ${theme.global.colors["text-on-brand"].dark};
	
	span {
		font-size: 10pt;
		display: none;
		@media screen and (min-width: 800px) {
			display: inline-block;
		}
	}
`

export function Sidebar() {
	const dispatch = useDispatch();
	const login = useSelector((state: RootState) => state.login);

	return (
		<GrommetCustomSidebar background="brand" pad={"none"}>
			<SidebarButton href="/" text={"home"} icon={<Home />} />
			<SidebarButton href="/about" text={"about"} icon={<CircleQuestion />} />
			{login.data?.role.canAuditLog && <SidebarButton href="/auditLog" text={"audit log"} icon={<ShieldSecurity />} />}
			{login.data?.role.canCreateSubject && <SidebarButton href="/subject/create" text={"create new subject"} icon={<Add />} />}
			{login.data?.role.canSuperviseSubject && <SidebarButton href="/translation/create" text={"add content"} icon={<Add />} />}
			{login.data != null && <SidebarButton onClick={() => dispatch(handleLogout())} text="log out" icon={<Logout />} />}
		</GrommetCustomSidebar>
	)
}

function SidebarButton(props: { text: string, icon: any; href?: string; onClick?: any }) {
	const location = useLocation();
	const isActive = location.pathname === props.href;

	const component = (
		<Box>
			<StyledButton active={isActive} onClick={props.onClick} hoverIndicator>
				<TextWithIcon text={props.text} icon={props.icon} />
			</StyledButton>
		</Box>
	);

	if (props.href != null) {
		return (
			<Link to={props.href}>
				{component}
			</Link>
		)
	}

	return component;
}