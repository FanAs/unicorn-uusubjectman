import {Box, Table, TableBody, TableCell, TableHeader, TableRow} from "grommet"
import styled from "styled-components"
import {TAuditLog} from "../../../../src/api/models/TAuditLog"
import React from "react";

const OverflowTableCell = styled(TableCell)`
	overflow: scroll;
    text-overflow: clip;
    white-space: normal;
    word-break: break-all;
    font-size: 10pt;
`;

export const AuditLog = ({data}: {data: TAuditLog[]}) => {
	return (
		<Box>
			<Table>
				<TableHeader>
					<TableCell scope="col" border="bottom">id</TableCell>
					<TableCell scope="col" border="bottom">event</TableCell>
					<TableCell scope="col" border="bottom">data</TableCell>
					<TableCell scope="col" border="bottom">user</TableCell>
					<TableCell scope="col" border="bottom">time</TableCell>
				</TableHeader>
				<TableBody>
					{data.map((auditLog) => (
						<TableRow>
							<TableCell scope="row">{auditLog.id}</TableCell>
							<TableCell scope="row">{auditLog.event}</TableCell>
							<OverflowTableCell scope="row" size="medium"><pre>{JSON.stringify(JSON.parse(auditLog.log), null, 2)}</pre></OverflowTableCell>
							<TableCell scope="row">{auditLog.user.id == null ? 'no user' : `${auditLog.user.firstName} ${auditLog.user.lastName} (${auditLog.user.role?.id} role)`}</TableCell>
							<TableCell scope="row">{new Date(auditLog.time * 1000).toISOString().slice(0, 19).replace('T', ' ')}</TableCell>
						</TableRow>
					))}
				</TableBody>
			</Table>
		</Box>
	)
}