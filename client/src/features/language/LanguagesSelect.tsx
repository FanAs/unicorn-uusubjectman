import React, { Component } from "react";

import { Box, TextInput } from "grommet";
import styled from "styled-components"
import {TLanguage} from "../../../../src/api/models/TLanguage"
import {theme} from "../../styles/theme"

import Language from "./Language";

const renderLanguage = (language: TLanguage, onRemove?: (language: TLanguage) => void) => {
	return (
		<Box align="center" direction="row" wrap={true} pad={{ left: "xsmall" }}>
			<Language key={language.id} onRemove={onRemove == null ? null : () => onRemove(language)}>
				{language.name}
			</Language>
		</Box>
	);
};

const StyledTextInput = styled(TextInput)`
	box-shadow: 0 0 1px 1px ${theme.global.colors.border.light};
	&:focus {
		box-shadow: 0 0 1px 1px ${theme.global.colors.brand.light};
		border: none;
	}
`

export class LanguagesSelect extends Component<{ disabled?: boolean, suggestions: TLanguage[], value: TLanguage | null, onRemove?: (language: TLanguage) => void, onSelect: (language: TLanguage) => void }> {
	static defaultProps = {
		suggestions: [],
		value: []
	};
	state = {
		search: ""
	};
	render() {
		const { suggestions, value, onRemove, onSelect, disabled } = this.props;
		const { search } = this.state;

		return (
			<Box
				wrap={true}
				direction="row"
				align="center"
				round="xsmall"
				pad="xxsmall"
			>
				{value != null && renderLanguage(value, onRemove)}
				<Box
					alignSelf="stretch"
					align="start"
					flex={true}
					style={{ minWidth: "240px" }}
				>
					{!disabled && value == null && <StyledTextInput
						plain={true}
						placeholder="Select language"
						type="search"
						value={search}
						onChange={({ target: { value: search } }) =>
							this.setState({ search })
						}
						onSelect={({ suggestion }) =>
							this.setState(
								{
									search: ""
								},
								() => {
									const language = suggestions.find((language) => language.name === suggestion);
									if (language != null) {
										onSelect(language);
									}
								},
							)
						}
						suggestions={suggestions.filter((suggestion) => suggestion.name.toLowerCase().indexOf(search.toLowerCase()) >= 0).map((language) => language.name)}
					/>}
				</Box>
			</Box>
		);
	}
}
