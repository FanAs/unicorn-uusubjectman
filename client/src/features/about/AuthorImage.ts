import {Image} from "grommet"
import styled from "styled-components"
import {theme} from "../../styles/theme"

export const AuthorImage = styled(Image)`
	border-radius: 50%;
	object-fit: cover;
	width: 120px;
	height: 120px;
	border: 4px solid ${theme.global.colors.brand.light};
`
