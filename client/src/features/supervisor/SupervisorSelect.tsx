import React, { Component } from "react";

import { Box, TextInput } from "grommet";
import styled from "styled-components"
import {TUser} from "../../../../src/api/models/TUser"
import {theme} from "../../styles/theme"

import Supervisor from "./Supervisor";

const renderSupervisor = (supervisor: TUser & {name: string}, onRemove?: (supervisor: TUser & {name: string}) => void) => {
	return (
		<Box align="center" direction="row" wrap={true} pad={{ left: "xsmall" }}>
			<Supervisor key={supervisor.id} onRemove={onRemove == null ? null : () => onRemove(supervisor)}>
				{supervisor.name}
			</Supervisor>
		</Box>
	);
};

const StyledTextInput = styled(TextInput)`
	box-shadow: 0 0 1px 1px ${theme.global.colors.border.light};
	&:focus {
		box-shadow: 0 0 1px 1px ${theme.global.colors.brand.light};
		border: none;
	}
`

export class SupervisorSelect extends Component<{ disabled?: boolean, suggestions: TUser[], value: TUser | null, onRemove?: (supervisor: TUser) => void, onSelect: (supervisor: TUser) => void }> {
	static defaultProps = {
		suggestions: [],
		value: []
	};
	state = {
		search: ""
	};
	render() {
		const { suggestions, value, onRemove, onSelect, disabled } = this.props;
		const { search } = this.state;

		const modifiedSuggestions = suggestions.map((suggestion) => ({
			...suggestion,
			name: `${suggestion.firstName} ${suggestion.lastName}`,
		}));

		let modifiedValue: TUser & {name: string} | null = value as any;
		if (modifiedValue != null) {
			modifiedValue.name = `${modifiedValue.firstName} ${modifiedValue.lastName}`;
		}

		return (
			<Box
				wrap={true}
				direction="row"
				align="center"
				round="xsmall"
				pad="xxsmall"
			>
				{modifiedValue != null && renderSupervisor(modifiedValue, onRemove)}
				<Box
					alignSelf="stretch"
					align="start"
					flex={true}
					style={{ minWidth: "240px" }}
				>
					{!disabled && modifiedValue == null && <StyledTextInput
						plain={true}
						placeholder="Select supervisor"
						type="search"
						value={search}
						onChange={({ target: { value: search } }) =>
							this.setState({ search })
						}
						onSelect={({ suggestion }) =>
							this.setState(
								{
									search: ""
								},
								() => {
									const supervisor = modifiedSuggestions.find((supervisor) => supervisor.name === suggestion);
									if (supervisor != null) {
										onSelect(supervisor);
									}
								},
							)
						}
						suggestions={modifiedSuggestions.filter((suggestion) => suggestion.name.toLowerCase().indexOf(search.toLowerCase()) >= 0).map((degree) => degree.name)}
					/>}
				</Box>
			</Box>
		);
	}
}
