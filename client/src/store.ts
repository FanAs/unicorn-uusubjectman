import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { Action, combineReducers } from "@reduxjs/toolkit";
import { ThunkAction } from "redux-thunk";
import {loginReducer} from "./features/login/loginSlice"

export const rootReducer = combineReducers({
	login: loginReducer,
});

const store = configureStore({
	devTools: process.env.NODE_ENV !== "production",
	reducer: rootReducer,
	middleware: getDefaultMiddleware({})
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>;
export type AppDispatch = typeof store.dispatch;

export { store };