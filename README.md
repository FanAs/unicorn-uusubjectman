Study project

1) Run database and adminer using docker-compose:
```bash
docker-compose up
```

2) Run server (in project root):
```
npm start
```

3) Run client using (in `client` folder):
```
npm start
```