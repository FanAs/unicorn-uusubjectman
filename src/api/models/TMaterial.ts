export type TMaterial = {
	id?: number;
	url: string;
	name: string;
	type: string;
}
