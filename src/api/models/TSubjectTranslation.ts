import {TDegree} from "./TDegree"
import { TLanguage } from "./TLanguage";
import {TMaterial} from "./TMaterial"
import {TSubject} from "./TSubject"
import {TUser} from "./TUser"

export type TSubjectTranslation = {
	id: number;
	subject: TSubject;
	language: TLanguage;
	name: string;
	goal: string;
	description: string;
	materials: TMaterial[];
}