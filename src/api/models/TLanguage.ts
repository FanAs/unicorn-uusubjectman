export type TLanguage = {
	id: number;
	name: string;
	localName: string;
}