import {TRole} from "./TRole"

export type TUser = {
	id: number;
	email: string;
	firstName: string;
	lastName: string;
	role: TRole
}
