export type TRole = {
	id: number;
	canCreateSubject: boolean;
	canSuperviseSubject: boolean;
	canAuditLog: boolean;
}
