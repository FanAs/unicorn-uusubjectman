import {TDegree} from "./TDegree"
import {TUser} from "./TUser"

export type TSubject = {
	id: number;
	name: string;
	isRequired: boolean;
	credits: number;
	supervisor: TUser;
	degree: TDegree;
}