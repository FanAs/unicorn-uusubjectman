import {TUser} from "./TUser"

export type TAuditLog = {
	id: number;
	log: string;
	event: string;
	time: number;
	user: TUser;
}
