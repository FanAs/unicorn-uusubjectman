import {StatusCodes} from "http-status-codes"

export class HttpError extends Error {
	public readonly statusCode: StatusCodes;

	public constructor(statusCode: StatusCodes, message: string) {
		super(message);
		Object.setPrototypeOf(this, HttpError.prototype);

		this.statusCode = statusCode;
	}
}
