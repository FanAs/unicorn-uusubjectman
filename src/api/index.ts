import cookieSession from "cookie-session"
import express from "express"
import {StatusCodes} from "http-status-codes"
import {config} from "../config"
import {TRole} from "./models/TRole"
import {registerRoutes} from "./routes"

const API_KEY = process.env.API_KEY;
/**
 * Starts rest server for REST API and static pages (for React application)
 */
export const startServer = (): Promise<void> => {
	const server = express();

	server.use(cookieSession({
		name: 'session',
		secret: config.cookie.secret,
	}));

	server.use((req, res, next) => {
		if (req.headers['api-key'] != null) {
			if (req.headers['api-key'] === API_KEY) {
				req['userInfo'] = {
					id: -1,
					email: 'api@uusubjectman.com',
					firstName: 'API',
					lastName: 'API',
					role: {
						id: -1,
						canCreateSubject: true,
						canSuperviseSubject: true,
						canAuditLog: true,
					}
				}
			} else {
				res.status(StatusCodes.FORBIDDEN);
				res.json({error: 'Invalid API key'});
				res.end();
				return;
			}
		} else {
			const json = req['session']['userInfo'];
			if (json == null) {
				next();
				return null;
			}

			req['userInfo'] = JSON.parse(json);
		}

		next();
	});

	registerRoutes(server);

	return new Promise<void>((resolve) => {
		server.listen(config.port, () => {
			console.info(`Application is running on http://localhost:${config.port}/`);
			resolve();
		});
	})
}
