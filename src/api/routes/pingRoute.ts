import {Request} from "express"

/**
 * For application monitoring
 *
 * @param req
 */
export const pingRoute = async (req: Request): Promise<unknown> => ({ status: 'ok' })
