import cors from "cors"
import express, {json, Request, Response} from "express"
import {StatusCodes} from "http-status-codes"
import * as path from "path"
import {StructError} from "superstruct"
import {addAuditLog} from "../data/addAuditLog"
import {HttpError} from "../HttpError"
import {returnJson} from "../returnJson"
import {apiAuditLogGet} from "./api/apiAuditLogGet"

import {apiDefaultRoute} from "./api/apiDefaultRoute"
import {apiDegreesGet} from "./api/apiDegreesGet"
import {apiDeleteSubject} from "./api/apiDeleteSubject"
import {apiDeleteTranslation} from "./api/apiDeleteTranslation"
import {apiPatchSubject} from "./api/apiPatchSubject"
import {apiPatchTranslation} from "./api/apiPatchTranslation"
import {apiPostTranslation} from "./api/apiPostTranslation"
import {apiSetUserRole} from "./api/apiSetUserRole"
import { apiSubjectGet } from "./api/apiSubjectGet"
import {apiLanguagesGet} from "./api/apiLanguagesGet"
import {apiPostSubject} from "./api/apiPostSubject"
import {apiSubjectsGet} from "./api/apiSubjectsGet"
import {apiSupervisorsGet} from "./api/apiSupervisorsGet"
import {apiTranslationsGet} from "./api/apiTranslationsGet"
import {apiUserGetInfoRoute} from "./api/apiUserGetInfoRoute"
import {authGoogleRoute} from "./api/authGoogleRoute"
import {authLogoutRoute} from "./api/authLogoutRoute"
import {pingRoute} from "./pingRoute"
import {reactRoute} from "./reactRoute"
import { apiTranslationGet } from "./api/apiTranslationGet"

const routeWrapper = (route: (req: Request, res: Response) => Promise<unknown>) => async (req: Request, res: Response) => {
	try {
		const data = await route(req, res);
		if (data === undefined) {
			return;
		}

		returnJson(StatusCodes.OK, data, res);
	} catch (err) {
		if (err instanceof HttpError) {
			return returnJson(err.statusCode, { message: err.message, statusCode: err.statusCode }, res);
		}

		if (err instanceof StructError) {
			return returnJson(StatusCodes.BAD_REQUEST, { message: `Validation error (${err.message})` }, res);
		}

		addAuditLog(req['userInfo'], `Internal server error ${err.message}`, req.body);
		returnJson(StatusCodes.INTERNAL_SERVER_ERROR, { message: 'Internal server error' }, res)
	}
}

export const registerRoutes = (server: express.Express): void => {
	server.use(json({limit: '50mb'}));
	server.use(cors({}));

	server.use(express.static('client/build'));
	server.get('/ping', routeWrapper(pingRoute));
	server.post('/api', routeWrapper(apiDefaultRoute));
	server.get('/api/subjects', routeWrapper(apiSubjectsGet));
	server.get('/api/supervisors', routeWrapper(apiSupervisorsGet));
	server.post('/api/subject', routeWrapper(apiPostSubject));
	server.post('/api/translation', routeWrapper(apiPostTranslation))
	server.get('/api/languages', routeWrapper(apiLanguagesGet));
	server.get('/api/degrees', routeWrapper(apiDegreesGet));
	server.get('/api/auditLog', routeWrapper(apiAuditLogGet));
	server.patch('/api/subject/:id', routeWrapper(apiPatchSubject));
	server.patch('/api/translation/:id', routeWrapper(apiPatchTranslation));
	server.delete('/api/subject/:id', routeWrapper(apiDeleteSubject));
	server.delete('/api/translation/:id', routeWrapper(apiDeleteTranslation));

	server.get('/api/subject/:id',routeWrapper(apiSubjectGet));
	server.get('/api/translation/:id',routeWrapper(apiTranslationGet));
	server.get('/api/translations/:id',routeWrapper(apiTranslationsGet));

	server.get('/api/user/info', routeWrapper(apiUserGetInfoRoute));
	server.post('/api/user/setRole', routeWrapper(apiSetUserRole));

	
	server.use('/auth/google', routeWrapper(authGoogleRoute));
	server.post('/auth/logout', routeWrapper(authLogoutRoute));

	server.get('*', (req, res) =>     res.sendFile(path.resolve(process.cwd(), 'client', 'build', 'index.html')))
	server.all('/api/*', (req, res) => res.writeHead(StatusCodes.NOT_FOUND, 'Not found'));
}
