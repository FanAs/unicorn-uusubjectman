import {boolean, Infer, max, min, number, object, optional, size, string} from "superstruct"

export const apiPatchSubjectValidator = object({
	name: optional(size(string(), 1, 64)),
	credits: optional(max(min(number(), 1), 25)),
	degreeId: optional(min(number(), 1)),
	supervisorId: optional(min(number(), 1)),
	isRequired: optional(boolean()),
});

export type ApiPatchSubjectType = Infer<typeof apiPatchSubjectValidator>
