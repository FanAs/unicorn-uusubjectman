import {array, boolean, enums, Infer, min, number, object, optional, size, string} from "superstruct"

export const apiPatchTranslationValidator = object({
	name: optional(size(string(), 1, 64)),
	description: optional(size(string(), 1, 1024 * 512)),
	goal: optional(size(string(), 1, 256)),
	subjectId: optional(min(number(), 1)),
	languageId: optional(min(number(), 1)),
	materials: optional(size(array(object({
		id: optional(min(number(), 1)),
		name: size(string(), 1, 128),
		url: size(string(), 1, 256),
		type: enums(['uubook', 'uucourse', 'url', 'video']),
		delete: optional(boolean()),
	})), 0, 1000)),
});

export type ApiPatchTranslationType = Infer<typeof apiPatchTranslationValidator>
