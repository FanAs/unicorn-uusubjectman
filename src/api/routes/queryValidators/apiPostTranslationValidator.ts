import {array, boolean, enums, Infer, min, number, object, optional, size, string} from "superstruct"

export const apiPostTranslationValidator = object({
	name: size(string(), 1, 64),
	description: size(string(), 1, 1024 * 512),
	goal: size(string(), 1, 256),
	subjectId: min(number(), 1),
	languageId: min(number(), 1),
	materials: size(array(object({
		name: size(string(), 1, 128),
		url: size(string(), 1, 256),
		type: enums(['uubook', 'uucourse', 'url', 'video']),
		delete: optional(boolean()),
	})), 0, 1000),
});

export type ApiPostTranslationType = Infer<typeof apiPostTranslationValidator>
