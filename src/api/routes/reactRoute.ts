import express from "express"
import {resolve} from "path"

/**
 * Serves static routes for React application
 */
export const reactRoute = express.static(resolve('client', 'build'));
