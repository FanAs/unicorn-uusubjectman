import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {assert} from "superstruct"
import {addAuditLog} from "../../data/addAuditLog"
import {createTranslation} from "../../data/createTranslation"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"
import {apiPostTranslationValidator} from "../queryValidators/apiPostTranslationValidator"

export const apiPostTranslation = async (req: Request): Promise<unknown> => {
	const user: TUser = (req as any)['userInfo'];
	if (!user?.role.canSuperviseSubject) {
		addAuditLog(user, `forbidden to create new translation`, req.body);
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	assert(req.body, apiPostTranslationValidator);

	addAuditLog(user, `new translation created`, req.body);

	return createTranslation(req.body);
}
