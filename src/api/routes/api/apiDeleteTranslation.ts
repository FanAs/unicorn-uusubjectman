import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {array, assert, max, min, number, object, optional, size, string} from "superstruct"
import {numberString} from "../../../util/isNumberString"
import {addAuditLog} from "../../data/addAuditLog"
import {deleteTranslation} from "../../data/deleteTranslation"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"

const paramsValidator = object({
	id: numberString(1),
});

export const apiDeleteTranslation = async (req: Request): Promise<unknown> => {
	const user: TUser = req['userInfo'];
	if (!user?.role.canSuperviseSubject) {
		addAuditLog(user, `forbidden to delete a translation`, [req.body, req.params]);
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	assert(req.params, paramsValidator);

	addAuditLog(user, `translation deleted`, req.body);

	await deleteTranslation(Number(req.params.id));
	return {};
}
