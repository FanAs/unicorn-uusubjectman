import {Request} from "express"

export const authLogoutRoute = async (req: Request): Promise<unknown> => {
	delete req['session']['userInfo'];

	return { };
}
