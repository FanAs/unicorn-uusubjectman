import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {addAuditLog} from "../../data/addAuditLog"
import {getAuditLog} from "../../data/getAuditLog"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"

export const apiAuditLogGet = async (req: Request): Promise<unknown> => {
	const user: TUser = (req as any)['userInfo'];
	if (!user?.role.canAuditLog) {
		addAuditLog(user, `forbidden to audit log`, req.body);
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	addAuditLog(user, `viewing audit log`, null);

	return getAuditLog();
}
