import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {array, assert, max, min, number, object, optional, size, string} from "superstruct"
import {numberString} from "../../../util/isNumberString"
import {addAuditLog} from "../../data/addAuditLog"
import {deleteSubject} from "../../data/deleteSubject"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"

const paramsValidator = object({
	id: numberString(1),
});

export const apiDeleteSubject = async (req: Request): Promise<unknown> => {
	const user: TUser = req['userInfo'];
	if (!user?.role.canCreateSubject) {
		addAuditLog(user, `forbidden to delete a subject`, req.body);
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	assert(req.params, paramsValidator);

	addAuditLog(user, `subject deleted`, req.body);

	await deleteSubject(Number(req.params.id));
	return {};
}
