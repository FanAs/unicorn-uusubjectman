import {Request} from "express"
import {object, size, string, assert} from "superstruct"
import {config} from "../../../config"
import {OAuth2Client} from "google-auth-library"
import {addAuditLog} from "../../data/addAuditLog"
import {createUser} from "../../data/createUser"
import {getUser} from "../../data/getUser"

const queryValidator = object({
	token: size(string(), 1, 2048),
});

const client = new OAuth2Client(config.google.id)

export const authGoogleRoute = async (req: Request): Promise<unknown> => {
	assert(req.body, queryValidator);

	const { token }  = req.body;
	const ticket = await client.verifyIdToken({
		idToken: token,
		audience: config.google.id,
	});

	const user = ticket.getPayload();

	addAuditLog(null, `authenticating with Google`, user);

	try {
		const data = {
			email: user.email,
			firstName: user.given_name,
			lastName: user.family_name,
			accessId: user.sub,
			accessToken: user['jti'],
			role: config.defaultRole,
		};

		await createUser(data);

		addAuditLog(null, `new user was created`, data);
	} catch (err) {
	}

	const userInfo = await getUser({sub: user.sub});

	req['session']['userInfo'] = JSON.stringify(userInfo);

	return { user: userInfo };
}