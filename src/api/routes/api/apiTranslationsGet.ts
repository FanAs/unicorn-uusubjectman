import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {assert, object, optional} from "superstruct"
import {numberString} from "../../../util/isNumberString"
import {getTranslations} from "../../data/getTranslations"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"

const paramsValidator = object({
	id: optional(numberString(1)),
});

export const apiTranslationsGet = async (req: Request): Promise<unknown> => {
	const user: TUser = req['userInfo'];
	if (!user) {
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	assert(req.params, paramsValidator);

	const {id} = req.params;

	return getTranslations(Number(id));
}
