import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {assert, object} from "superstruct"
import {numberString} from "../../../util/isNumberString"
import {addAuditLog} from "../../data/addAuditLog"
import {updateTranslation} from "../../data/updateTranslation"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"
import {apiPatchTranslationValidator} from "../queryValidators/apiPatchTranslationValidator"

const paramsValidator = object({
	id: numberString(1),
});

export const apiPatchTranslation = async (req: Request): Promise<unknown> => {
	const user: TUser = (req as any)['userInfo'];
	if (!user?.role.canSuperviseSubject) {
		addAuditLog(user, `forbidden to update a translation`, req.body);
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	assert(req.params, paramsValidator);
	assert(req.body, apiPatchTranslationValidator);

	addAuditLog(user, `translation updated`, req.body);

	return updateTranslation(req.params.id, req.body);
}
