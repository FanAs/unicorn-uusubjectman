import {Request} from "express"
import {StatusCodes} from "http-status-codes"
import {assert, max, min, number, object, size, string} from "superstruct"
import {setUserRole} from "../../data/setUserRole"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"

const queryValidator = object({
	roleId: min(number(), 1),
});

export const apiSetUserRole = async (req: Request): Promise<unknown> => {
	const user: TUser = req['userInfo'];
	if (!user) {
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	assert(req.body, queryValidator);

	return setUserRole(req, req.body.roleId, req['userInfo']);
}
