import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {getSubjects} from "../../data/getSubjects"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"

export const apiSubjectsGet = async (req: Request): Promise<unknown> => {
	const user: TUser = req['userInfo'];
	if (!user) {
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	return getSubjects();
}
