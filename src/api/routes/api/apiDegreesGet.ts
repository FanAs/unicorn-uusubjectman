import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {assert, min, number, object, size, string} from "superstruct"
import {getDegrees} from "../../data/getDegrees"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"

export const apiDegreesGet = async (req: Request): Promise<unknown> => {
	const user: TUser = req['userInfo'];
	if (!user) {
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	return getDegrees();
}
