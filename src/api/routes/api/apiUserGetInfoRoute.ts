import {Request} from "express"

export const apiUserGetInfoRoute = async (req: Request): Promise<unknown> => req['userInfo'] ?? null;
