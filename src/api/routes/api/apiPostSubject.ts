import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {array, assert, boolean, max, min, number, object, optional, size, string} from "superstruct"
import {addAuditLog} from "../../data/addAuditLog"
import {createSubject} from "../../data/createSubject"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"

const queryValidator = object({
	name: size(string(), 1, 64),
	credits: max(min(number(), 1), 25),
	degreeId: min(number(), 1),
	supervisorId: min(number(), 1),
	isRequired: boolean(),
});

export const apiPostSubject = async (req: Request): Promise<unknown> => {
	const user: TUser = req['userInfo'];
	if (!user?.role.canCreateSubject) {
		addAuditLog(user, `forbidden to create new subject`, req.body);
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	assert(req.body, queryValidator);

	addAuditLog(user, `new subject created`, req.body);

	return createSubject(req.body);
}
