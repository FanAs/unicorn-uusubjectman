import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {array, assert, boolean, max, min, number, object, optional, size, string} from "superstruct"
import {numberString} from "../../../util/isNumberString"
import {addAuditLog} from "../../data/addAuditLog"
import {createSubject} from "../../data/createSubject"
import {updateSubject} from "../../data/updateSubject"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"

const paramsValidator = object({
	id: numberString(1),
});

const queryValidator = object({
	name: optional(size(string(), 1, 64)),
	credits: optional(max(min(number(), 1), 25)),
	degreeId: optional(min(number(), 1)),
	supervisorId: optional(min(number(), 1)),
	isRequired: optional(boolean()),
});

export const apiPatchSubject = async (req: Request): Promise<unknown> => {
	const user: TUser = req['userInfo'];
	if (!user?.role.canCreateSubject) {
		addAuditLog(user, `forbidden to update a subject`, req.body);
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	assert(req.params, paramsValidator);
	assert(req.body, queryValidator);

	addAuditLog(user, `subject updated`, req.body);

	return updateSubject(Number(req.params.id), req.body);
}
