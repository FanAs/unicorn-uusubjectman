import { Request } from "express"
import {StatusCodes} from "http-status-codes"
import {assert, object} from "superstruct"
import {numberString} from "../../../util/isNumberString"
import {getTranslation} from "../../data/getTranslation"
import {HttpError} from "../../HttpError"
import {TUser} from "../../models/TUser"

const paramsValidator = object({
	id: numberString(1),
});

export const apiTranslationGet = async (req: Request): Promise<unknown> => {
	const user: TUser = req['userInfo'];
	if (!user) {
		throw new HttpError(StatusCodes.FORBIDDEN, 'Forbidden');
	}

	assert(req.params, paramsValidator);

	const {id} = req.params;

	return getTranslation(Number(id));
}
