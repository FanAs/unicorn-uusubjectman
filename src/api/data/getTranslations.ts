import {mysql} from "../../data/mysql"
import {TSubjectTranslation} from "../models/TSubjectTranslation"
import {translationSqlQuery} from "./translationSqlQuery"

export const getTranslations = async (subjectId: number): Promise<TSubjectTranslation[]> => {
	const [rows]: [any[], any] = await mysql.connection.query(translationSqlQuery(`ss.id = ${subjectId}`));

	return rows;
}