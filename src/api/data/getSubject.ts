import {StatusCodes} from "http-status-codes"
import {mysql} from "../../data/mysql"
import {HttpError} from "../HttpError"
import {TSubject} from "../models/TSubject"
import {subjectSqlQuery} from "./subjectSqlQuery"

export const getSubject = async (subjectId: number): Promise<TSubject> => {
	const [rows]: [any[], any] = await mysql.connection.query(subjectSqlQuery(`ss.id = ${subjectId}`));

	if (rows.length === 0) {
		throw new HttpError(StatusCodes.NOT_FOUND, 'No such subject');
	}

	const row: TSubject = rows[0];

	row.isRequired = <any>row.isRequired === 1;

	return row;
}