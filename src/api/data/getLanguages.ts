import {mysql} from "../../data/mysql"
import {TLanguage} from "../models/TLanguage"

export const getLanguages = async (): Promise<TLanguage[]> => {
	const [rows] = await mysql.connection.query(`
		SELECT
			l.id AS id,
		    l.name AS name,
			l.localName AS localName
		FROM language AS l`,
	);

	return rows as TLanguage[];
}