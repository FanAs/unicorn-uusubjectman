import {mysql} from "../../data/mysql"
import {TSubject} from "../models/TSubject"
import {subjectSqlQuery} from "./subjectSqlQuery"

export const getSubjects = async (): Promise<TSubject[]> => {
	const [rows]: [any[], any] = await mysql.connection.query(subjectSqlQuery());

	return rows;
}