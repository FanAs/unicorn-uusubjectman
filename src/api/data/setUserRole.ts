import {mysql} from "../../data/mysql"
import {TRole} from "../models/TRole"
import {TUser} from "../models/TUser"
import {getUser} from "./getUser"

export const setUserRole = async (req, roleId: number, user: TUser) => {
	await mysql.connection.query(`
        UPDATE user
        SET role_id = ?
        WHERE user.id = ?`, [
		roleId,
		user.id,
	]);

	const userInfo = await getUser({id: user.id});

	req['session']['userInfo'] = JSON.stringify(userInfo);

	return userInfo;
};
