export const translationSqlQuery = (where?: string): string => `
		SELECT
			t.id AS id,
			t.name AS name,
			t.goal AS goal,
			t.description AS description,
			JSON_OBJECT(
				'id', l.id,
				'name', l.name,
				'localName', l.localName
			) AS language,
			JSON_ARRAYAGG(
				JSON_OBJECT(
					'id', m.id,
					'url', m.url,
					'name', m.name,
					'type', m.type
				)
			) AS materials,
			JSON_OBJECT(
				'id', ss.id,
				'name', ss.system_name,
				'credits', ss.credits,
				'degree', JSON_OBJECT(
					'id', d.id,
					'name', d.name
				),
				'supervisor', JSON_OBJECT(
					'id', u.id,
					'firstName', u.first_name,
					'lastName', u.last_name,
					'email', u.email
				)
			) AS subject
		FROM translation AS t
		JOIN study_subject AS ss ON (
			t.study_subject_id = ss.id
		)
		JOIN language AS l ON (
			t.language_id = l.id
		)
		JOIN degree AS d ON (
		    d.id = ss.degree_id
		)
		JOIN user AS u ON (
		    u.id = ss.supervisor_id
		)
		LEFT JOIN translation_has_material AS thm ON (
			t.id = thm.translation_id
		)
		LEFT JOIN material AS m ON (
			thm.material_id = m.id
		)
		${where == null ? '' : `WHERE ${where}`}
		GROUP BY t.id`;