import {mysql} from "../../data/mysql"
import {TUser} from "../models/TUser"
import {userSqlQuery} from "./userSqlQuery"

export const getSupervisors = async (): Promise<TUser[]> => {
	const [rows]: [any[], any] = await mysql.connection.query(userSqlQuery(`r.can_supervise_subject = 1`));

	return rows;
}