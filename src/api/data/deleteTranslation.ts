import {mysql} from "../../data/mysql"

export const deleteTranslation = async (translationId: number): Promise<void> => {
	await mysql.connection.query(`DELETE FROM translation WHERE id = ?`, [translationId]);
}
