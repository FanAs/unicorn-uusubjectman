import {mysql} from "../../data/mysql"
import {Role} from "../models/Role"

export const createUser = (user: {email: string, firstName: string, lastName: string, accessId: string, accessToken: string, role: Role}) => mysql.connection.query(`
				INSERT INTO user (email, first_name, last_name, access_id, access_token, role_id) VALUES (?, ?, ?, ?, ?, ?)
			`, [
	user.email,
	user.firstName,
	user.lastName,
	user.accessId,
	user.accessToken,
	user.role,
])
