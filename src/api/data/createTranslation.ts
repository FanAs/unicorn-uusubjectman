import {ResultSetHeader} from "mysql2"
import {mysql} from "../../data/mysql"
import {ApiPostTranslationType} from "../routes/queryValidators/apiPostTranslationValidator"
import {createMaterial} from "./createMaterial"
import {getTranslation} from "./getTranslation"

export const createTranslation = async (translation: ApiPostTranslationType): Promise<{ id: number }> => {
	await mysql.connection.beginTransaction();

	const [result] = await mysql.connection.query(`INSERT INTO translation (study_subject_id, language_id, name, description, goal) VALUE (?, ?, ?, ?, ?)`, [
		translation.subjectId,
		translation.languageId,
		translation.name,
		translation.description,
		translation.goal,
	]);

	const translationId = (<ResultSetHeader>result).insertId;

	const results = await Promise.all(translation.materials.map((material) => createMaterial(material)));

	await Promise.all(results.map((material) => mysql.connection.query(`INSERT INTO translation_has_material (translation_id, material_id) VALUE (?, ?)`, [translationId, material.id])));

	await mysql.connection.commit();

	return getTranslation(translationId);
}