import {mysql} from "../../data/mysql"
import {TLanguage} from "../models/TLanguage"

export const createLanguage = async (language: TLanguage) => {
	try {
		await mysql.connection.query('INSERT INTO language (name, localName) VALUE (?, ?)', [language.name, language.localName]);
	} catch (err) {
		if (err.code !== 'ER_DUP_ENTRY') {
			throw err;
		}
	}
}