import {mysql} from "../../data/mysql"
import {TDegree} from "../models/TDegree"

export const createDegree = async (degree: TDegree) => {
	try {
		await mysql.connection.query('INSERT INTO degree (name) VALUE (?)', [degree.name]);
	} catch (err) {
		if (err.code !== 'ER_DUP_ENTRY') {
			throw err;
		}
	}
}