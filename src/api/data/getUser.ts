import {mysql} from "../../data/mysql"
import {TUser} from "../models/TUser"
import {userSqlQuery} from "./userSqlQuery"

export const getUser = async (query: {sub?: string, id?: number}): Promise<TUser> => {
	const [userInfo] = await mysql.connection.query(userSqlQuery(query.sub == null ? `user.id = ${query.id}` : `access_id = '${query.sub}'`));

	const user: TUser = userInfo[0];

	user.role.canSuperviseSubject = <unknown>user.role.canSuperviseSubject === 1;
	user.role.canCreateSubject = <unknown>user.role.canCreateSubject === 1;
	user.role.canAuditLog = <unknown>user.role.canAuditLog === 1;

	return user;
}