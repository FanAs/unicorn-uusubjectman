import {mysql} from "../../data/mysql"
import {ApiPostTranslationType} from "../routes/queryValidators/apiPostTranslationValidator"
import {createMaterial} from "./createMaterial"
import {getTranslation} from "./getTranslation"

export const updateTranslation = async (translationId, translation: ApiPostTranslationType): Promise<{ id: number }> => {
	await mysql.connection.beginTransaction();

	const set = [];
	const values = [];

	if (translation.name !== undefined) {
		set.push(`name = ?`);
		values.push(translation.name);
	}
	if (translation.description !== undefined) {
		set.push(`description = ?`);
		values.push(translation.description);
	}
	if (translation.goal !== undefined) {
		set.push(`goal = ?`);
		values.push(translation.goal);
	}
	if (translation.languageId !== undefined) {
		set.push(`language_id = ?`);
		values.push(translation.languageId);
	}

	const where = [`id = ?`];
	values.push(translationId);

	if (set.length > 0) {
		await mysql.connection.query(`UPDATE translation SET ${set.join(', ')} WHERE ${where.join(' AND ')}`, values);
	}

	if (translation.materials !== undefined && translation.materials.length > 0) {
		await mysql.connection.query(`DELETE FROM translation_has_material WHERE translation_id = ?`, [translationId]);

		const results = await Promise.all(translation.materials.map((material) => createMaterial(material)));

		await Promise.all(results.map((material) => mysql.connection.query(`INSERT INTO translation_has_material (translation_id, material_id) VALUE (?, ?)`, [translationId, material.id])));
	}

	await mysql.connection.commit();

	return getTranslation(translationId);
}