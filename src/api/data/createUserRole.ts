import {mysql} from "../../data/mysql"
import {TRole} from "../models/TRole"

export const createUserRole = async (role: TRole) => {
	try {
		await mysql.connection.query('INSERT INTO role (id, can_create_subject, can_supervise_subject, can_audit_log) VALUE (?, ?, ?, ?)', [role.id, role.canCreateSubject ? 1 : 0, role.canSuperviseSubject ? 1 : 0, role.canAuditLog ? 1 : 0]);
	} catch (err) {
		if (err.code !== 'ER_DUP_ENTRY') {
			throw err;
		}
	}
}