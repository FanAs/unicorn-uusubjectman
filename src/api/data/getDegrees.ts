import {mysql} from "../../data/mysql"
import {TDegree} from "../models/TDegree"

export const getDegrees = async (): Promise<TDegree[]> => {
	const [rows] = await mysql.connection.query(`
		SELECT
			d.id AS id,
		    d.name AS name
		FROM degree AS d`,
	);

	return rows as TDegree[];
}