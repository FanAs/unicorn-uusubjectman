import {mysql} from "../../data/mysql"
import {TAuditLog} from "../models/TAuditLog"

export const getAuditLog = async (): Promise<TAuditLog[]> => {
	const [rows] = await mysql.connection.query(`
		SELECT
            al.id AS id,
            al.log AS log,
            UNIX_TIMESTAMP(al.time) AS time,
			al.event AS event,
            JSON_OBJECT(
                'id', u.id,
                'firstName', u.first_name,
                'lastName', u.last_name,
                'email', u.email,
                'role', JSON_OBJECT(
                    'id', r.id,
                    'canCreateSubject', r.can_create_subject,
                    'canSuperviseSubject', r.can_supervise_subject,
                    'canAuditLog', r.can_audit_log
                )
            ) AS user
		FROM audit_log AS al
		LEFT JOIN user AS u ON (
		    al.user_id = u.id
		)
        LEFT JOIN role AS r ON (
        	u.role_id = r.id
        )
        ORDER BY al.id DESC`,
	);

	return rows as TAuditLog[];
}