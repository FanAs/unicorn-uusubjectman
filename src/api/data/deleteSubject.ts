import {mysql} from "../../data/mysql"

export const deleteSubject = async (subjectId: number): Promise<void> => {
	await mysql.connection.query(`DELETE FROM study_subject WHERE id = ?`, [subjectId]);
}