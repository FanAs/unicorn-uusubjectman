export const subjectSqlQuery = (where?: string): string => `
		SELECT
			ss.id AS id,
			ss.system_name AS name,
			ss.credits AS credits,
			ss.is_required AS isRequired,
			JSON_OBJECT(
				'id', d.id,
				'name', d.name
			) AS degree,
			JSON_OBJECT(
				'id', u.id,
				'firstName', u.first_name,
				'lastName', u.last_name,
				'email', u.email
			) AS supervisor
		FROM study_subject AS ss
		JOIN degree AS d ON (
		    d.id = ss.degree_id
		)
		JOIN user AS u ON (
		    u.id = ss.supervisor_id
		)
		${where == null ? '' : `WHERE ${where}`}
		GROUP BY ss.id`;