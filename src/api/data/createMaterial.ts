import {ResultSetHeader} from "mysql2"
import {mysql} from "../../data/mysql"
import {TMaterial} from "../models/TMaterial"

export const createMaterial = async (material: TMaterial): Promise<TMaterial> => {
	const [result] = await mysql.connection.query(`INSERT INTO material (url, name, type) VALUE (?, ?, ?)`, [material.url, material.name, material.type]);

	return {
		id: (<ResultSetHeader>result).insertId,
		...material,
	};
}
