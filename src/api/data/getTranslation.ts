import {StatusCodes} from "http-status-codes"
import {mysql} from "../../data/mysql"
import {HttpError} from "../HttpError"
import {TSubjectTranslation} from "../models/TSubjectTranslation"
import {translationSqlQuery} from "./translationSqlQuery"

export const getTranslation = async (translationId: number): Promise<TSubjectTranslation> => {
	const [rows]: [any[], any] = await mysql.connection.query(translationSqlQuery(`t.id = ${translationId}`));

	if (rows.length === 0) {
		throw new HttpError(StatusCodes.NOT_FOUND, 'No such translation');
	}

	const row: TSubjectTranslation = rows[0];
	row.materials = row.materials.filter((material) => material.id != null);

	return rows[0];
}