import {ResultSetHeader} from "mysql2"
import {mysql} from "../../data/mysql"
import {TSubject} from "../models/TSubject"
import {getSubject} from "./getSubject"

export const createSubject = async (subject: {
	name: string;
	credits: number;
	degreeId: number;
	supervisorId: number;
	isRequired: boolean;
}): Promise<TSubject> => {
	const [result] = await mysql.connection.query(`INSERT INTO study_subject (system_name, credits, degree_id, supervisor_id, is_required) VALUE (?, ?, ?, ?, ?)`, [
		subject.name,
		subject.credits,
		subject.degreeId,
		subject.supervisorId,
		subject.isRequired ? 1 : 0,
	]);

	const subjectId = (<ResultSetHeader>result).insertId;

	return getSubject(subjectId);
}