import {mysql} from "../../data/mysql"
import {TUser} from "../models/TUser"

const getCurrentDateTime = (): string => new Date().toISOString().slice(0, 19).replace('T', ' ');

export const addAuditLog = (user: TUser, event: string, additionalInfo: any) => {
	mysql.connection.query(`INSERT INTO audit_log (log, time, event, user_id) VALUE (?, ?, ?, ?)`, [JSON.stringify(additionalInfo), getCurrentDateTime(), event, user?.id ?? null]).catch((err) => {
		console.error(err);
	});
}