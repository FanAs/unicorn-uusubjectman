import {mysql} from "../../data/mysql"
import {ApiPatchSubjectType} from "../routes/queryValidators/apiPatchSubjectValidator"
import {getSubject} from "./getSubject"

export const updateSubject = async (subjectId: number, subject: ApiPatchSubjectType): Promise<{ id: number }> => {
	const set = [];
	const values = [];

	if (subject.name !== undefined) {
		set.push(`system_name = ?`);
		values.push(subject.name);
	}
	if (subject.credits !== undefined) {
		set.push(`credits = ?`);
		values.push(subject.credits);
	}
	if (subject.degreeId !== undefined) {
		set.push(`degree_id = ?`);
		values.push(subject.degreeId);
	}
	if (subject.supervisorId !== undefined) {
		set.push(`supervisor_id = ?`);
		values.push(subject.supervisorId);
	}
	if (subject.isRequired !== undefined) {
		set.push(`is_required = ?`);
		values.push(subject.isRequired ? 1 : 0);
	}

	const where = [`id = ?`];
	values.push(subjectId);

	await mysql.connection.query(`UPDATE study_subject SET ${set.join(', ')} WHERE ${where.join(' AND ')}`, values);

	return getSubject(subjectId);
}