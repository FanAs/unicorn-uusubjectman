export const userSqlQuery = (where?: string): string => `
		SELECT
			user.id AS id,
			user.email AS email,
			user.first_name AS firstName,
			user.last_name AS lastName,
			JSON_OBJECT(
				'id', r.id,
				'canCreateSubject', r.can_create_subject,
			    'canSuperviseSubject', r.can_supervise_subject,
			    'canAuditLog', r.can_audit_log
			) AS role
		FROM user
		JOIN role AS r ON (
			r.id = user.role_id
		)
		${where == null ? '' : `WHERE ${where}`}`;