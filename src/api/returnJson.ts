import {ServerResponse} from "http"
import {StatusCodes} from "http-status-codes"

export const returnJson = (statusCode: StatusCodes, value: any, res: ServerResponse): void => {
	res.writeHead(statusCode, { 'Content-Type': 'application/json' });
	res.end(JSON.stringify(value));
};
