import {config} from "../config"
import {Connection, createConnection} from "mysql2/promise"

export const mysql = {
	connection: <Connection>undefined,
	init: async (): Promise<void> => {
		mysql.connection = await createConnection(config.mysql);
	}
}