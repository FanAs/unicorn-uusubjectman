import {createDegree} from "../api/data/createDegree"
import {createLanguage} from "../api/data/createLanguage"
import {createUserRole} from "../api/data/createUserRole"
import {Role} from "../api/models/Role"
import {mysql} from "./mysql"

export const createDbStructure = async (): Promise<void> => {
	await mysql.connection.query('CREATE DATABASE IF NOT EXISTS uu_subject_man');

	await mysql.connection.query(`CREATE TABLE IF NOT EXISTS uu_subject_man.role (
		id INT UNIQUE NOT NULL AUTO_INCREMENT,
		can_create_subject TINYINT(1),
		can_supervise_subject TINYINT(1),
        can_audit_log TINYINT(1),
		PRIMARY KEY (id)
    )`);

	await mysql.connection.query(`CREATE TABLE IF NOT EXISTS uu_subject_man.material (
         id INT UNIQUE NOT NULL AUTO_INCREMENT,
         url TINYTEXT,
         name TINYTEXT,
         type ENUM('video', 'uubook', 'uucourse', 'url')
	)`);

	await mysql.connection.query(`CREATE TABLE IF NOT EXISTS uu_subject_man.language (
		id INT UNIQUE NOT NULL AUTO_INCREMENT,
		name VARCHAR(32) UNIQUE,
		localName VARCHAR(32)
    )`);

	await mysql.connection.query(`CREATE TABLE IF NOT EXISTS uu_subject_man.degree (
        id INT UNIQUE NOT NULL AUTO_INCREMENT,
        name VARCHAR(16) UNIQUE
    )`);

	await mysql.connection.query(`CREATE TABLE IF NOT EXISTS uu_subject_man.user (
		id INT UNIQUE NOT NULL AUTO_INCREMENT,
		email VARCHAR(128) NOT NULL,
		first_name VARCHAR(64) NOT NULL,
		last_name VARCHAR(64) NOT NULL,
		access_id VARCHAR(255) UNIQUE,
		access_token VARCHAR(40),
		role_id INT NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE
    )`);

	await mysql.connection.query(`CREATE TABLE IF NOT EXISTS uu_subject_man.study_subject (
		id INT UNIQUE NOT NULL AUTO_INCREMENT,
		system_name VARCHAR(64) UNIQUE,
		is_required TINYINT(1),
		credits SMALLINT,
		degree_id INT NOT NULL,
		supervisor_id INT NOT NULL,
		PRIMARY KEY (id),
		FOREIGN KEY (degree_id) REFERENCES degree(id) ON DELETE CASCADE,
		FOREIGN KEY (supervisor_id) REFERENCES user(id) ON DELETE CASCADE
	 )`);

	await mysql.connection.query(`CREATE TABLE IF NOT EXISTS uu_subject_man.translation (
        id INT UNIQUE NOT NULL AUTO_INCREMENT,
		study_subject_id INT NOT NULL,
		language_id INT NOT NULL,
        name VARCHAR(64) NOT NULL,
		description TEXT,
        goal TINYTEXT,
        PRIMARY KEY (id),
        FOREIGN KEY (study_subject_id) REFERENCES study_subject(id) ON DELETE CASCADE,
        FOREIGN KEY (language_id) REFERENCES language(id) ON DELETE CASCADE
    )`);

	await mysql.connection.query(`CREATE TABLE IF NOT EXISTS uu_subject_man.translation_has_material (
		translation_id INT NOT NULL,
		material_id INT NOT NULL,
		FOREIGN KEY (translation_id) REFERENCES translation(id) ON DELETE CASCADE,
		FOREIGN KEY (material_id) REFERENCES material(id) ON DELETE CASCADE
    )`);

	await mysql.connection.query(`CREATE TABLE IF NOT EXISTS uu_subject_man.audit_log (
		id INT UNIQUE NOT NULL AUTO_INCREMENT,
		log TEXT,
		time DATETIME,
		event VARCHAR(256),
		user_id INT,
		FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE
    )`)

	await Promise.all([
		createUserRole({id: Role.ADMIN, canCreateSubject: true, canSuperviseSubject: true, canAuditLog: true }),
		createUserRole({id: Role.USER, canCreateSubject: false, canSuperviseSubject: true, canAuditLog: false }),
		createLanguage({id: null, name: 'English', localName: 'English'}),
		createLanguage({id: null, name: 'Czech', localName: 'čeština'}),
		createDegree({id: null, name: 'Bachelor (Bc.)'}),
		createDegree({id: null, name: 'Master (Mgr.)'}),
	])
}
