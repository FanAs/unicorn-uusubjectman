import {readFile} from "fs"

const readFileAsync = (path: string): Promise<Buffer> => new Promise((resolve, reject) => {
	readFile(path, (err, data) => {
		if (err) {
			return reject(err);
		}

		resolve(data);
	})
})

export const readMock = async (filename: string): Promise<any> => {
	const data = await readFileAsync(`${__dirname}/../../mocks/${filename}.json`);

	return JSON.parse(data.toString('utf-8'));
}