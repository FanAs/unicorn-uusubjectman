import {startServer} from "./api";
import {createDbStructure} from "./data/createDbStructure"
import {mysql} from "./data/mysql"

(async () => {
	await Promise.all([mysql.init()]);
	await Promise.all([createDbStructure()]);
	await startServer();
})().catch((err) => {
	console.error(`Unexpected error: ${err}`);
	process.exit(1);
});
