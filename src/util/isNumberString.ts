import {define} from "superstruct"

const isNumberString = (value: any, min?: number, max?: number): boolean => {
	if (typeof value !== 'string') {
		throw new Error(`Expect string but got ${typeof value}`);
	}

	const num = Number(value);
	if (num.toString() !== value) {
		throw new Error(`Expect value to be a number bot got ${value}`);
	}

	if (num < min) {
		throw new Error(`Expect value to be greater than ${min} but got ${num}`);
	}

	if (num > max) {
		throw new Error(`Expect value to be less than ${max} but got ${num}`);
	}

	return true;
}

export const numberString = (min?: number, max?: number) => define<string>('numberString', (value) => isNumberString(value, min, max));
