export const ignoreUndefined = <T>(obj: T): T => {
	const keys = Object.keys(obj);
	const resultObject: any = Object.create(null);

	for (const key of keys) {
		if (obj[key] === undefined) {
			continue;
		}

		resultObject[key] = obj[key];
	}

	return resultObject;
}
