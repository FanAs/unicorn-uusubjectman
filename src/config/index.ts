/**
 * We don't store config data (like resource handles) in code, instead we use environment variables
 * @see https://12factor.net/config
 */
import { config as dotenvConfig } from 'dotenv';
import { resolve } from 'path';

// project .env
dotenvConfig({path: resolve(process.cwd(), '.env')});

// default .env
dotenvConfig({path: resolve(process.cwd(), 'config', 'default.env')});

// production .env
dotenvConfig({path: resolve(process.cwd(), 'config', 'production.env')});

export const config = {
	port: Number(process.env.APP_PORT),
	google: {
		id: process.env.GOOGLE_CLIENT_ID,
		secret: process.env.GOOGLE_CLIENT_SECRET,
		callbackUrl: process.env.GOOGLE_CLIENT_CALLBACK_URL
	},
	mysql: {
		host: process.env.MYSQL_HOST,
		port: Number(process.env.MYSQL_PORT),
		user: process.env.MYSQL_USER,
		password: process.env.MYSQL_PASS,
		database: process.env.MYSQL_DATABASE,
	},
	cookie: {
		secret: process.env.COOKIE_SECRET,
	},
	defaultRole: Number(process.env.DEFAULT_ROLE),
}
